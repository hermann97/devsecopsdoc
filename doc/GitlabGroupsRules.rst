Gitlab
######

Gitlab RACI
===========




.. tabularcolumns:: |\X{3}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|

+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Tasks                                         | ICT mngr     | ICT team         | Infra mngr    | Infra team         | DevSecOps Guild | team devsecops leaders | team developers |
+===============================================+==============+==================+===============+====================+=================+========================+=================+
| Setup/maintain/backup Gitlab                  |  A           |      R           |          C    |        I           |        C        |             I          |                 | 
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Define usage inside DevSecOps Infra           |  I           |      I           |         RA    |        R           |        C        |             I          |          I      |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Configure Gitlab application                  |  I           | C  (review)      |          A    |        R           |        I        |       I                |                 |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Create groups under RD-ALL                    |              | C  (review)      |          A    |        R           |        I        |       I                |                 |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Maintain groups under RD-ALL                  |              |                  |          A    |        R           |                 |       I                |                 |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Create 2nd level of groups                    |              |                  |               | I                  |                 |       RA               |   I             |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Create projects                               |              |                  |               |                    |                 |        A               |  R              |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Maintain groups of 2nd level (assign rights)  |              |                  |               |        I           |                 |        RA              |                 |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+

Note team decsecops leaders are owners of the subgroups they manage.

Gitlab Groups
=============

Groups in gitlab are used to

- regroup projects

- associate users and roles

- associate runners

- regroup packages repositories

- ...

At Ekinops, for R&D, we have created a main group called rd-all that is linked to the GR_RD_ALL Ekinops group. So all R&D is part of this group and have a Developer role.

GR-SALES group is added to rd-all with reporter role. See https://docs.gitlab.com/ee/user/permissions.html for more information about roles.

No project will be created under this group. No manual add of user. New owners can be added by Arch&Infra team. The role of those owners is to add new groups if needed.

Then under this main group, several sub-groups are created to
- regroup several projects
- give specific owner roles to those subgroup

The mail role of those subgroups' Owners is to add maintener roles to underlaying projects (also remove, change name of projects...)

The main group is rd-all. In this group, all Ekinops R&D has a developper role.

.. list-table:: rd-all subgroups
   :widths: 25 25
   :header-rows: 1

   * - Sub groups
     - owners
   * - Access-Meta
     - Christopher Raucy, Filip Van Rillaer
   * - CSO
     - To be named
   * - DesignStudio
     - Alain Enout, Vincent Jardin, Jean-François Roux
   * - DevTools
     - Didier Lesage, Marc Michot, John Sucaet, Guillaume Azerad
   * - EkinopsArchive
     - Marc Michot, Sisihlia Yuniyarti, Guillaume Azerad
   * - FTE
     - Thierry Royer
   * - Gitops team
     - Mayeul Mathias, Alain Enout
   * - HLD
     - Didier Lesage, Marc Michot
   * - Legacy
     - Guillaume Azerad, Marc Michot
   * - LicenseFactory
     - Srinivasa Raghavan, Marc Michot, Didier Lesage
   * - NFV-Misc
     - Alain Enout, Vincent Jardin
   * - oatDevTools
     - Srinivasa Raghavan, Sisihlia Yuniyarti, Guillaume Azerad, Marc Michot, Didier Lesage
   * - OneManage
     - Vincent Jardin, Mandar Kallol
   * - OneOs5
     - To be named
   * - OneOs6
     - Filip Van Rillaer, Alain Enout, Mayeul Mathias
   * - OneOsTesting
     - David Callaert, Shreeram Bhat Puthige
   * - OneOsWebGUI
     - Mandar Kallol, Vincent Jardin
   * - OVPOneOs
     - Alain Enout, Vincent Jardin
   * - ResearchArchitecture
     - Didier Lesage, Marc Michot
   * - SDWAN
     - Koen Schoofs, Fabrice Gamberini, Mandar Kallol, Vincent Jardin
   * - TDRE
     - To be named
   * - Templates
     - Didier Lesage, Guillaume Azerad, marc Michot, John Sucaet
   * - Transports
     - Myrwan Ayed, Rhenan Nascimento
   * - WX
     - Alain Enout, Vincent Jardin

A second group, rd-restricted, contains project with restricted access. By default, no one has access. Some specific AD groups are created and mapped to subgroups.

.. list-table:: rd-restricted subgroups
   :widths: 25 25
   :header-rows: 1

   * - Sub groups
     - owners
   * - NFV
     - GR-ADH-RD-GITLAB-NFV_RESTRICTED

A third group exists; RD-Subcontractor to give access to subcontractor to a set of project. Users can be mapped to that group, and projects under rd-all can be shared under that group.


Creation of new projects
========================

Gitlab is setup, so that (if admin of group did not change default values) any Developers can create a project. 
In term of ownership, new projects inherit from group ownership. So the creator of a project is not the owner of the project. 
See https://docs.gitlab.com/ee/user/permissions.html for permission details.

If the creator of the project needs more access rights, it is up to him to contact one of the subgroup owners. 
Subgroup owners can raise the role of the project creator (either to Maintainer or Owner), or can change the settings of the newly created project.

In case of OneOs6 modules, after the creation, the subgroup owner must change:
  - Settings/General/Merge requet approvals : Eligible users increase "Approvals required" from 0 to 1
  - Settings/Repository/Protected branches : For branch "main" change the "Allowed to merge" from "Maintainers" to "Maintainers + Developers"


Docker registry
===============

Gitlab acts as a docker registry. Registry can be enabled on a project, or on a group. See https://docs.gitlab.com/ee/user/packages/container_registry/.

Untagged, or unreferenced images are regularly cleaned. This appends every Sunday at 10pm (CET). During a few minutes, the registry is not reachable.

Administrative operations
=========================

.. toctree::
  :maxdepth: 1

  GitlabUpgradeProcedures





