Upgrade procedures
------------------

It takes 5 minutes for the gitlab to be accessible at the time.

They are stil downtime while upgrading. The site is not reachable for a minutes. https://forum.gitlab.com/t/gitlab-zero-downtime-upgrade-inquiry/49354/2 and https://microfluidics.utoronto.ca/gitlab/help/update/zero_downtime.md#geo-deployment

Before running all those commands you have to connected to the server via ssh.

Refer to https://microfluidics.utoronto.ca/gitlab/help/update/zero_downtime.md#geo-deployment for complete details about upgrade/downgrade procedures. You will find in this page a summary.

Important note, Gitlab will not be available during the upgrade/downgrade operation. Furthermore, the operation can take some time due to the backup of the database. As Gitlab is used by hundredd of users, care must be taken to do this operation when the impact is the lower and the operation must be advertized in advance.

In order to have low risk during those operation, it must first be done on the acceptance platform. 

We also follow this documentation in other to do the upgrade without downtime.

Back up before upgrading (To be done before doing any step)
^^^^^^^^^^^^^^^^^^^^^^^^

The GitLab database is backed up before installing a newer GitLab version. 

But you can also run this manual command to do the backup of the database before upgrading:

If you are using Omnibus 12.2 or newer, use :
::

 gitlab-backup create

If you are using Omnibus 12.2 or older, use :
::

 gitlab-rake gitlab:backup:create

Upgrade path
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In other to do the upgrade you need to follow this link: https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/ so that you will all the step to update to a new version.

* you need to select enterprise edition and zero downtime as the image :

.. image:: images/upgradepath.PNG

* After selecting the version you want you will have an order of all the upgrade that need to be done before.

.. image:: images/upgradepath1.PNG

* verify that migrations are all completed when migrations are done with the commmands :

::

 sudo gitlab-rails runner -e production 'puts Gitlab::BackgroundMigration.remaining'

 sudo gitlab-rails runner -e production 'puts Gitlab::BackgroundMigration.pending'

* General Migrations Check
::

 sudo gitlab-rake db:migrate:status


Downgrade (ex: 14.10.x to 13.10.y)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Firstly to downgrade the version of gitlab-ee you have to to stop and remove the package of the current version with the command 

::

  sudo dpkg -r gitlab-ee

* After that you have to identify the version you want to downgrade with the command :

::

  sudo apt-cache madison gitlab-ee

* You cannot downgrade directly from 14.10.X to 13.10.y. You need to install firstly the 14.0.X :

::
 
 sudo apt install gitlab-ee=14.0.X...
 
 after that run the command 

::
 
  sudo apt install gitlab-ee=13.10.X... to install the package.

* And after restart gitlab with the command :

::
  
  sudo gitlab-ctl restart

* link of gitlab documentation: https://docs.gitlab.com/ee/update/package/downgrade.html

Major and Minor update (ex : 14.x to 14.y and 14.x.x to 14.x.y)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Please read https://docs.gitlab.com/ee/update/package/#version-specific-changes before starting the upgrade. In some case, specific operation must be done before the upgrade.

1. Before that, migrations should be complete before each upgrade!
::
  sudo gitlab-rails runner -e production 'puts Gitlab::BackgroundMigration.remaining'

  and

  sudo gitlab-rails runner -e production 'puts Gitlab::BackgroundMigration.pending'

2. General Migrations Check
::
  sudo gitlab-rake db:migrate:status

3. Firstly to update from minor version of gitlab-ee , you have to check the version of your gitlab with the command: 
::

  sudo gitlab-rake gitlab:env:info

4. After that you have to choose the version of gitlab you want. The command :
::

  sudo apt-cache madison gitlab-ee

5. Create an empty file at /etc/gitlab/skip-auto-reconfigure. This prevents upgrades from running gitlab-ctl reconfigure, which by default automatically stops GitLab, runs all database migrations, and restarts GitLab.
::
  sudo touch /etc/gitlab/skip-auto-reconfigure

6. Edit /etc/gitlab/gitlab.rb and ensure the following is present:  
::
  gitlab_rails['auto_migrate'] = false

7. Reconfigure GitLab:
::
 sudo gitlab-ctl reconfigure

8. After that run this command to install the package: 
::

 sudo apt install gitlab-ee=<version>

9. To get the database migrations and latest code in place, run :
::
  sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=true gitlab-ctl reconfigure

10. Hot reload puma and sidekiq services
::
  sudo gitlab-ctl hup puma
  sudo gitlab-ctl restart sidekiq

* link of gitlab documentation: https://docs.gitlab.com/ee/update/package/

On each secondary node run first the command in the top ( from 1 to 6) for the primary node in te top and executing the following:
Database needs to be back up.
::
 sudo SKIP_POST_DEPLOYMENT_MIGRATIONS=true gitlab-ctl reconfigure

Hot reload puma, sidekiq and restart geo-logcursor services
::
 sudo gitlab-ctl hup puma
 sudo gitlab-ctl restart sidekiq
 sudo gitlab-ctl restart geo-logcursor

Run post-deployment database migrations, specific to the Geo database
::

 sudo gitlab-rake geo:db:migrate

Run post-deployment database migrations
::
  sudo gitlab-rake db:migrate

After updating all nodes (both primary and all secondaries), check their status:

Verify Geo configuration and dependencies
::
  sudo gitlab-rake gitlab:geo:check

After all secondary nodes are updated, finalize the update on the primary node:

Run post-deployment database migrations
::
  sudo gitlab-rake db:migrate

After updating all nodes (both primary and all secondaries), check their status:

Verify Geo configuration and dependencies
::
  sudo gitlab-rake gitlab:geo:check

Updating Gitlab runners
^^^^^^^^^^^^^^^^^^^^^^^

For updating the gitlab runner you need to run those commands:

Below is the way to update the Gitlab runner version:

::

  # Pull the latest version (or a specific tag):
  sudo docker pull gitlab/gitlab-runner:latest
 
  # Stop and remove the existing container:
  sudo docker stop gitlab-runner && docker rm gitlab-runner
 
  # Start the container as you did originally:
  sudo docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest

Link of the documentation : https://ekipedia.ekinops.com/pages/viewpage.action?spaceKey=RDDev&title=How+to+create+a+Gitlab+runner

In case of any modification in /srv/gitlab-runner/config/config.toml file, you will have to restart the Gitlab runner container this way:
::
  sudo docker restart gitlab-runner

Updating the geo replica
^^^^^^^^^^^^^^^^^^^^^^^^

To upgrade the Geo sites when a new GitLab version is released, upgrade primary and all secondary sites:

* Optional. Pause replication on each secondary site to protect the disaster recovery (DR) capability of the secondary sites.
* SSH into each node of the primary site. 
* Upgrade GitLab on the primary site.
* Perform testing on the primary site, particularly if you paused replication in step 1 to protect DR. There are some suggestions for post-upgrade testing in the upgrade documentation.
* SSH into each node of secondary sites.
* Upgrade GitLab on each secondary site.
* If you paused replication in step 1, resume replication on each secondary. Then, restart Puma and Sidekiq on each secondary site. This is to ensure they are initialized against the newer database schema that is now replicated from the previously upgraded primary site. sudo gitlab-ctl restart sidekiq & sudo gitlab-ctl restart puma.

* link of documentation : https://docs.gitlab.com/ee/administration/geo/replication/upgrading_the_geo_sites.html

Updating Postgresql
^^^^^^^^^^^^^^^^^^^

* To do after an upgrade of gitlab version (if stated in the release note)

* To upgrade postgresql use the command : sudo gitlab-ctl pg-upgrade 

* Run the command :

::

 sudo gitlab-psql -c "SELECT relname, last_analyze, last_autoanalyze FROM pg_stat_user_tables WHERE last_analyze IS NULL AND last_autoanalyze IS NULL;" to check if everything is working fine.

* clean old database with the command:

::

 sudo rm -rf /var/opt/gitlab/postgresql/data.<old_version> 

 and 

 sudo rm -f /var/opt/gitlab/postgresql-version.old




