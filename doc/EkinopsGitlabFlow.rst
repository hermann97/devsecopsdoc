Ekinops gitlab flow
*********************

Introduction
############
Ekinops gitlab flow, summarizes the process that has been adopted in the DevSecOps transformation. Ekinops flow, is a derived from widely used *Gitlab flow*.

.. drawio-image:: ./graphics/ekiflow.drawio
  :format: png



The Ekinops flow
################


Highlights of Ekinops gitlab flow

1. Fork the project in your own space.
2. Create feature branch
3. Make changes and commit it.
4. Raise draft merge request, when part of the feature/bug is ready for review. This will also trigger check pipeline runners.
5. Make changes based on review comments and/or continue with changes
6. Rebase and clean commit history, if needed.
7. Publish the merge request.
8. If changes are requested by the reviewer, repeat step 5-6. Else, merge request is marked as approved.
9. If main branch has changed, rebase the feature branch, also resolve any merge conflict that may occur
10. Gate pipeline will be triggered, on any push done after merge request approval. If no push is done, you can manually run the pipeline to execute the gate pipeline.
11. Merge to main using fast-forward method, if gate pipeline is successful. Else, reopen merge request.
12. Release pipeline is triggered which will add version tag, build and deploy artefact(s).

Prerequisites
#############

* Having git installed and configured with Gitlab credentials
* Basic git knowledge
* Concept of branch and merge
* Understanding of existing workflow (any)

Git initialization
------------------

If git is not installed (it should be natively done on most Linux distributions), please check this `page <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`_ to do so on your correponding OS.

Then, in order to be able to synchronize your local git repository with the remote one from Gitlab Ekinops instance,
you will have to configure git with your Gitlab credentials.

Below, we will explain how to **enable https cloning from Gitlab**. If you prefer using ssh, you can create your own ssh key
and add it into the `SSH keys section <https://gitlab.ekinops.com/-/profile/keys>`_ of your personal user settings.

Generate a Gitlab token
^^^^^^^^^^^^^^^^^^^^^^^

Go to the *Acces Tokens* section from your personal user settings : https://gitlab.ekinops.com/-/profile/personal_access_tokens

.. image:: images/git_init1.JPG
  :width: 700

Then check the *api* scope which will give you enough rights to publish on your Gitlab projects.

Now, you can generate the access token and save it somewhere.

Configure git to store the credentials
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now we will see how to update your local git configuration to be able to store your credentials
which allow you to connect to Gitlab, depending on your OS.

Linux
"""""

We recommend to use `libsecret <https://wiki.gnome.org/Projects/Libsecret>`_ in order to store your credentials encrypted.

.. code:: bash
  
  sudo apt-get install libsecret-1-0 libsecret-1-dev
  
  # On Fedora, libsecret binaries are into this folder:
  # /usr/libexec/git-core/git-credential-libsecret
  cd /usr/share/doc/git/contrib/credential/libsecret
  sudo make

  # Update git global configuration credential helper field
  git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret

Windows
"""""""

No need here to install additional softwares, only update git configuration to store
your credentials into the Windows Credential Manager.

.. code:: bash
  
  git config --global credential.helper manager-core

If you use **WSL** and want to use git inside the Linux distribution installed, please
modify the previous command as below:

.. code:: bash

  git config --global credential.helper /mnt/c/Program\ Files/Git/mingw64/libexec/git-core/git-credential-manager-core.exe

Register your credentials by cloning a project
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now, retrieve a Gitlab projet git clone command from the main page in Gitlab UI,
execute it on your local server and enter in this order:

* Your mail address (under which you are registerd on Ekinops Gitlab instance)
* The Gitlab token previously generated when the password is asked.

Once this is done, you will not be asked your credentials any more to connect to Gitlab instance.



Example of development workflow
-------------------------------

For instance, assuming the fork of `https://gitlab.ekinops.com/vincent.jardin/devsecopsdoc`, you can
contribute using:

::

  git clone git@gitlab.ekinops.com:vincent.jardin/devsecopsdoc.git
  git checkout -b gitHandson
  git config core.editor vi
  vim doc/EkinopsGitlabFlow.rst
  # update content
  git add doc/EkinopsGitlabFlow.rst
  git commit
    :git handson example # first line is mandatory, title of this git commit
    :                    # 2nd line shall be empty
    :Record the commands that have been used for updating this file.
    :It can be used as a cut+paste reference for a quick start.
  git push  # note that it does not work, but git provides a tip about the proper command
  git push --set-upstream origin gitHandson


Then, on gitlab, you can initiate the merge request.


Fixup a merge request
---------------------

Assuming you have some comments and your merge request needs to be udpated, you can apply the following:

::

  vim doc/EkinopsGitlabFlow.rst
  # update content v2
  git commit
    :git handson example for fixup # first line is mandatory, title of this git commit
    :                              # 2nd line shall be empty
    :Record the commands that have been used for updating this file.
    :It can be used as a cut+paste reference for fixing a commit.
  git push


fix failed merge request - rebase
---------------------------------

It is assumed that any merge requests shall be *fast foward* only. When the main repository
has been updated, it can be required to update the main repo using a rebase process.

For instance, it means:

::

  git remote -v
  git remote add upstream git@gitlab.ekinops.com:rd-all/devtools/devsecopsdoc.git
  git fetch upstream
  git checkout main # or master depending on the default main or master name
  git merge upstream/main # or master, let's apply the content from upstream
  git checkout gitHandson
  git merge --ff origin/main # do not forge --ff to avoid a merge commit
  git push


Flow details
############

.. toctree::
   :maxdepth: 1
   

   flow/featurebranch
   flow/commitchanges
   flow/mergerequest
   flow/rebase
   flow/fforward
   flow/policy
   flow/check
   flow/gate
   flow/release


Useful Git/Gitlab tutorials
############################

* `<https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging>`_
* `<https://ekinops.udemy.com/course/git-version-control/>`_
* `<https://ekinops.udemy.com/course/git-for-geeks/>`_
* `<https://ekinops.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/>`_
* `<https://ekinops.udemy.com/course/git-and-github-bootcamp/>`_
* `<https://ekinops.udemy.com/course/git-github-practical-guide/>`_
* `<https://ekinops.udemy.com/course/git-and-github-complete-guide/>`_


