    :Date: |today|
    :Author: **Didier LESAGE**, **Guillaume AZERAD**
    :Status: not approved
    



Static code Analysis Tool (SAST)
################################

Sonarqube has been selected for the SAST tooling. To access project scan : https://sonarqube.ekinops.com

The Entreprise version with a 5M code license is installed

It is used to scan in priority Ekinops code. Opensource code must be scanned only if remaining LOC of the license is sufficient.

Scan is done on main, in the release pipeline in order to have a status of a release. During a merge request, 
scan is executed and compared to the previous main version, so developper will see only added issues, and be able to fix them before
code is merged in main.

Security rules are set by the Security manager (Milas).


RACI (sonarqube.ekinops.com)
****************************

Admin (ICT and Infra) are responsible to maintain the server up, up to date and secure.

Security admins are responsible of defining security rules and gates.

Infra define rules in order to be able to share a unique instance for all the projects and R&D. See next paragraph.

R&D teams, must apply the rules when creating new project, or doing scans. Sonarqube must be used for all project in the release and Merge Request pipelines. 
Sonarqube badge must be used in gitlab project to give an overviez of the project status. Result of a scan during a Merge request must be published in Gitlab Merge request.
Private project (project in private namespace) must not be scanned due to the priority given to official project (limitation in number of line due to the license).

Rules
*****

There must be one sonarqube project per Gitlab project (for projects containing Ekinops code). In case several projects are linked to build an delivrable, 
a Sonarqube Application must be created to have a consolidated view of the delivrable. An application per R&D team can also be created to give "team view".

When creating a project, either through the GUI, or with a first scan, it is mandatory that the
    - sonarqube project key exactly match the Gitlab project path slug (`$CI_PROJECT_PATH_SLUG <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_): for example, rd-all-devtools-devsecopsdoc
    - sonarqube project name is the Gitlab path of the project: for example "rd-all/devtools/devsecopsdoc"

To do so, if you are using sonarscanner, please configure *sonar-project.properties* file at the root of your project, as below:

::

  sonar.projectKey=rd-all-devtools-devsecopsdoc
  sonar.projectName=rd-all/devtools/devsecopsdoc
  sonar.qualitygate.wait=true

`sonar.qualitygate.wait=true` is optional but recommended because it ensures that the Gitlab pipeline job fails if the sonar analysis launched fails.
You can add any other sonar property you need, among the ones defined `here <https://docs.sonarqube.org/latest/analysis/analysis-parameters/>`_.

It is also possible to dynamically generate the *sonar-project.properties* file into your .gitlab-ci.yml file, before launching the sonar scan :

::
  
  - echo "sonar.projectKey=$CI_PROJECT_PATH_SLUG" > $CI_PROJECT_DIR/sonar-project.properties
  - echo "sonar.projectName=$CI_PROJECT_PATH" >> $CI_PROJECT_DIR/sonar-project.properties
  - echo "sonar.qualitygate.wait=true" >> $CI_PROJECT_DIR/sonar-project.properties

If you are using the maven sonarscanner, as described `here <https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-maven/>`_, please add in your maven *settings.xml* file a property section like:

::

  <settings>
    <pluginGroups>
        <pluginGroup>org.sonarsource.scanner.maven</pluginGroup>
    </pluginGroups>
    <profiles>
        <profile>
            <id>sonar</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <sonar.projectKey>
                  rd-all-devtools-devsecopsdoc
                </sonar.projectKey>
                <sonar.projectName>
                  rd-all/devtools/devsecopsdoc
                </sonar.projectName>
                <sonar.qualitygate.wait>
                  true
                </sonar.qualitygate.wait>
            </properties>
        </profile>
     </profiles>
  </settings>

Then, add other required properties in the command line of your sonar scan as they are provided as Gitlab variables: 

::

  - mvn clean verify sonar:sonar -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_TOKEN

Sonarqube project creation
**************************

The Sonarqube project creation can either be done from Sonarqube UI, or automatically from a sonar scanner launched by a Gitlab pipeline.

Gitlab pipeline (recommended method)
====================================

Following the sonar scanner configuration previously defined, Sonarqube project is **automatically created** during the first Gitlab scan job executed.

.. image:: images/Sonarqube1.jpg

Then, you have to edit from Sonarqube UI the **default branch** name as it is automatically set as *master*.

To do so, please click on the project as displayed in the previous image, then select on the right top *Project Settings* drop-down list -> *Branches & Pull Requests*. Select the edit button of your project in the *Action* column, and click on *Rename branch*.

.. image:: images/Sonarqube2.jpg

Finally, set Gitlab devops platform integration in order to display your Quality Gate status directly in Gitlab UI.
Now, select *General Settings* in *Project Settings* drop-down list, then *DevOps Platform Integration* tab in the left side menu.
Here, select *Gitlab Ekinops* as configuration name, set the Gitlab project ID (displayed on Gitlab project page), and click on *Save* button.

.. image:: images/Sonarqube3.jpg

Sonarqube UI (second choice)
============================

To create a project from Sonarqube UI, go to the *Projects* page, *Create Project* drop-down list and click on *Gitlab*.

.. image:: images/Sonarqube4.jpg

Then, you might be asked to provide a **Gitlab token** if not done into your profile, so paste your Gitlab token and validate.

Now, you have to find and select your Gitlab project from the list displayed.

.. image:: images/Sonarqube5.jpg

The project is now created on Sonarqube, and you can click on *With Gitlab CI* icon to get help to initiate your sonar scan on Gitlab.

.. image:: images/Sonarqube6.jpg

But before launching your first scan, **update the project key** : go to *Project Settings* drop-down list -> *Update key*. Then, set the project path slug as defined in the `previous paragraph <#rules>`_.

.. image:: images/Sonarqube7.jpg

Finally, the **project name** will be edited during the first scan launched from Gitlab, so please set the related sonar property accordingly with the `rules <#rules>`_.

Sonarqube badges
****************

To display sonarqube project badges on Gitlab, you will first have to get the URL of the metric badge you want to be displayed on your Gitlab project page.

To do so, go to your Sonarqube project and *Project information* on the right top, then click on *Get project badges*.

.. image:: images/Sonarqube8.jpg

On this menu, you can select the *metric* you want to show from the drop-down list, and also select *Image URL only* as format to get the link that will be pasted into the Gitlab badge.

Then, got to your Gitlab project -> Settings -> General and expand *Badges* tab.

.. image:: images/Sonarqube9.jpg

Now, set the badge name you want (referring to the metric chosen for instance), the URL as your project dashboard one on Sonarqube for example, and finally paste the image URL previously copied from Sonarqube into the *Badge image URL* section.

When all of this is done, you will be able to see the badge on your Gitlab project page.

.. image:: images/Sonarqube10.jpg

Sonarqube scan and Ekinops Gitlab Flow
**************************************

"Sonarqube scans" must be executed on main branches (and release branches) to have a reference state of each delivered version. So must be part, at least, of the *release* pipeline.
Goal is to know the status of the release, know the technical debt and status of quality gates.

"Sonarqube scans" must be executed during a merge request, so in the *check* pipeline. Sonarqube will bublish in the merge request the result of the scan ( only the difference of the local branch and main version).
So during the merge request workflow, developpers and reviewers can fix any new introduced issues, and deliver in main a clean code to avoid increasing the technical dept.


Scan your code
**************

There is no unique method to scan the code, it mainly depends and the language used by the project. 
Sonarqube documentation is here: https://docs.sonarqube.org/latest/analysis/languages/overview/.

For example, C code (https://docs.sonarqube.org/latest/analysis/languages/cfamily/) must be done in two steps: building a database using the Build Wrapper, then running the sonar-scanner.

You can find here implementation on Ekinops project.

C code example
==============

gitlab project: TDB by Infra when doing 1st OneOs6 project

Comments:

JAVA example
============

gitlab project: https://gitlab.ekinops.com/rd-all/transport/celestis/celestis-nms

Comments:

PHP example
===========

gitlab project: 

Comments:

Python example
==============

gitlab project: 

Comments:

Setup
*****

Sonarqube is installed as a docker container and deployed with docker-compose, along with the related PostgreSQL database. In the mean time, a Nginx reverse proxy that handle HTTPS is deployed.

Users are managed with the Ekinops Azure AD. Two specifics group of admins, and security admins are synchronized from GR-ADH-RD-SONARQ-ADM and GR-ADH-RD-SONARQ-SEC. 
The first one are the Sonarqube administrators. The second one, the person that can change security rules and gates.

