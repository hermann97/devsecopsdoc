Internal artefact repository
############################

Internal artefact repository is used to
- proxy external thirdparty artefact used in projects ( npm packages, debian packages, docker images, ... )
- store official release artefacts ( could be intermediate if the final artefact is built from a set of intermediate artefacts). Those artemacts are build by the release pipeline on main and maintenances branches
- store temporary devlopment artefacts ( generated during check and gate pipelines)

Note 'Gitlab artefacts' is an other nothion. It is used by gitlab to share files between jobs of a pipeline. It will not be used to store artefact to be released to customers.

Internal artefact repository (Nexus) RACI
=========================================




.. tabularcolumns:: |\X{3}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|\X{1}{10}|

+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Tasks                                         | ICT mngr     | ICT team         | Infra mngr    | Infra team         | DevSecOps Guild | team devsecops leaders | team developers |
+===============================================+==============+==================+===============+====================+=================+========================+=================+
| Setup/maintain/backup Nexus server            |  A           |      R           |          C    |        I           |        C        |             I          |                 | 
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Define usage inside DevSecOps Infra           |  I           |      I           |         RA    |        R           |        C        |             I          |          I      |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Configure Nexus application                   |  I           | C  (review)      |          A    |        R           |        I        |       I                |                 |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Create a proxy repository                     | I (sec)      |     C (sec)      |          C    | C (disk)           |                 |       RA               |   I             |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Create/setup an internal repository           |              |                  |          I    | C (disk      )     |                 |        RA              |    I            |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+
| Maintain a repository (cleaning, rights, ...) |              |                  |               |        I           |                 |        RA              |                 |
+-----------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+


 
Usage of Nexus inside Ekinops DevSecOps infrastructure
======================================================

Nexus users
-----------

Ekinops logins and credentials are used for GUI access. As this version of Nexus does not handle creation of tocken, local account will be created to handle automatic access. So only the password of those local account will be stored in config files in gitlab or on developer PCs.

Two local users will be created by Gitlab subgroups of RD-ALL. A read access user called <subgroup>_nexus_read and a write access user called <subgroup>_nexus_write. Write access user is only used in pipelines ( to push temporary or released artefacts). Write users MUST not be used outside automatic pipelines. ( release of artefacts is only done by automatic system, not from a developper PC). Read users can be used in the pipelines, but also by developers to download artefacts on their PCs. So only the password of read users will be knonw to all R&D

Repositories
------------

In term of repository, there is 3 kinds of repositories : proxy, internal and temporary.

- Proxy are repositories used to proxy public repositories. 
- Internal are repositories for our own artefacts published from release branches ( officially tags of main or maintenance branches). Those artefacts are never cleaned. ( at least not an automatic clean based on the life time)
- temporary are repositories for temporary artefacts, like those generated from developpers branches, temporary test versions, ....  Those repositories MUST have automatic cleaning rules ( after month for example)

Like displayed in the RACI, each team has the responsibility over its repositories. Meaning that they manage the creation, cleaning, ... Infra mist be consulted for repository creation in order to manage correctly the disk space. Also, ICT will be consulted by Infra on proxy repository to manage any security risks.






