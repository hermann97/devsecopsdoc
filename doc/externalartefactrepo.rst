    :Date: |today|
    :Author: **Didier LESAGE**
    :Status: not approved
    :ReviewsOK: Stijn, Milas, Brice



External artefact repository
############################

External artefact repository is used to

- store official release artefacts for Ekinops customers who are using automatic DevSecOps tooling.


External artefact repository RACI (gitlab-ext)
**********************************************




+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+
| Tasks                                                      | ICT mngr     | ICT team         | Infra mngr    | Infra team         | DevSecOps Guild | team devsecops leaders | team developers |  CSO                 | Customer |
+============================================================+==============+==================+===============+====================+=================+========================+=================+======================+==========+
| Setup/Secure/maintain/backup Gitlab server                 |  A           |      R           |          C    |        I           |        C        |             I          | I (maintenance) |   I                  |          |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+
| Configure Gitlab application                               |  I           | C  (review)      |          A    |        R           |        I        |       I                |                 |                      |          |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+
| Create/setup an external repository                        |              |                  |          I    | C (disk      )     |                 |        RA              |    I            |   C                  |          |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+
| Maintain a repository (cleaning, rights, ...)              |              |                  |               |        I           |                 |        RA              |                 |    C                 |          |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+
|  Add / Delete an external user / sign customer certificate |              |       A          |               |                    |                 |         I              |                 | R                    |          |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+
| Publish a new artefact                                     |              |                  |               |                    |                 |         RA             |                 |    C                 |          |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+     
|  Manage Access tokens                                      |              |                  |               |                    |                 |                        |                 |                      |   RA     |
+------------------------------------------------------------+--------------+------------------+---------------+--------------------+-----------------+------------------------+-----------------+----------------------+----------+     

Characteristics
***************

Solution
========

Artefact repository feature is done using Gitlab solution. A Gitlab server is exposed. Gitlab  manages the users authentication, provides a GUI to manage tokens, acts as an artefact repository.


Dimensioning (compute/storage/network)
=======================================

NAME : SR-B0-GITLAB-EX

CPU : 4 cores

Memory : 8GB

Disk : 100GB

Network : 10.0.60.10 (DMZ)

External IP : 91.183.184.113




Deployment Diagram and integration in existing infrastructures
**************************************************************


.. image:: images/externalartefact.png


Security
********

Network
=======


Networking Flows
----------------

Allowed ports, zones, and initiators. Only this list must be authorized.

+-------------------------------------+---------------------------------+---------------------------------+
|            Usage                    | internal <-> server (initiator) | external <-> server (initiator) |
+=====================================+=================================+=================================+
|  push packages from internal gitlab | 443 (internal)                  |                                 |
+-------------------------------------+---------------------------------+---------------------------------+
|  management of gitlab (gui + ssh)   | 22 and 443 (internal)           |                                 |
+-------------------------------------+---------------------------------+---------------------------------+
|  customer managing his account      |                                 |  443 (external)                 |
+-------------------------------------+---------------------------------+---------------------------------+
|  customer pulling a package         |                                 | 443 (external)                  |
+-------------------------------------+---------------------------------+---------------------------------+
|  monitoring zabbix                  |  ???? ( server )                |                                 |
+-------------------------------------+---------------------------------+---------------------------------+
|  internal user logging mngt         | ldaps ?  ( server ? )           |                                 |
+-------------------------------------+---------------------------------+---------------------------------+

.. warning ::

    Anything missing ?

    Do we control ports based on internal or external zone ?
    
    DO we just control ports, or port + initiator ?



Network Security
----------------

WAF
^^^

No WAF in place. A WAF should be put in place in order to enhance the security. Need to be planned by ICT.

A rate limiter per IP per period for unauthenticated requests in place in Gitlab ( Gitlab feature )



Two way SSL ( MTLS )
^^^^^^^^^^^^^^^^^^^^

Principe of two way SSL :
  - Clients will send Hello and request for the resources on the secure HTTPS protocol.
  - The server will respond with its public certificate (.crt) and send Hello.
  - The client will verify the server public certificate in its truststore.
  - The client sends back symmetric session key generated using the server public certificate.
  - The server will decrypt the symmetric session key using the server private certificate and request for the client certificate.
  - The client will send its public certificate to the server and the server will verify the client public certificate in the server truststore.
  - The server will generate a session key and encrypt using the client public certificate and send it to the client.
  - The client will decrypt the session key using client private certificate and this way the key exchange between client and server. It will establish secure communication between client and server.


Front end of Gitlab is NGINX. SSL is manage by this software.

A certificate win a SN containing \*.ekinops.com and a SAN cantaining gitlab-ext.ekinops.com is configured on the server together with the private key corresponding to this certificate. This certificate is signed by a public CA. Clients can validate the identity of the gitlab-ext.ekinops.com server.

gitlab-ext.ekinops.com is configured to check the presence and validity of client certificates. To do so, a list of trusted certificate is adder to the /etc/gitlab/ssl/client_cert.crt" files ( containing Ekinops intermediate certificate). Gitlab is configured with the options ssl_verify_client = on and ssl_verify_depth=2. 

gitlab ['custom_gitlab_server_config'] = "ssl_crl=..." can be added to manage a list of client certificate to exclude.

To be able to only accept a list of known clients, we are using an Ekinops internal Root CA and intermediate CA. 

Our customers will generate a CSR ( Certificate Signing Request. RSA with key length of 2048 minimum is mandatory ). A certificate with a maximum validity of two years will be created from this CSR, using the Ekinops PKI with the intermediate root certificate and give back to the customer. Information of this certificate will be stored in our PKI infrastructure in order to be able to add this certificate to the CRL list.

Our customers will use this certificate to conctact the gitlab-ext.ekinops.com server ( browser or API ). Only connection initiated with certificates signed by Ekinops CA authority will be accepted.

Note that, gitlab cannot make a distinction between internal connection and public connection. It means that internal user must also use a signed certificate to access this server. This is not an issue as the access must be done by the release pipeline of the main gitlab instance, so only this instance must have a certificate. 

Only specific internal users that need to manage creation of new repositories will need a certificate. 

This limitation could be removed by adding an external reverse proxy with a config allowing ssl client verification only when request comes from external addresses.

.. warning::

    Do we handle CSR or do we create the customer client certificate. 
    
    What is our PKI ? Were do we secure our key ? Do we use HSM and same root key than device certificates ( but another intermediate certificate ?)

    Do we reuse same intermediate CA than the one used for factory test bench ( as ICT already have the process to generate such certificates )
    
    What is the process to ask for generation of a customer certificate or to revoke a customer certificate ?




.. code-block::

    gitlab-ext.ekinops.com 

    Certificate chain
    0 s:/C=FR/L=LANNION/O=EKINOPS SA/CN=*.ekinops.com
    i:/C=US/O=DigiCert Inc/CN=DigiCert TLS RSA SHA256 2020 CA1
    -----BEGIN CERTIFICATE-----                                                                      
    MIIGuTCCBaGgAwIBAgIQA+uwUz+O2fUfJZYd/P2pIzANBgkqhkiG9w0BAQsFADBP               
    MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMSkwJwYDVQQDEyBE
    aWdpQ2VydCBUTFMgUlNBIFNIQTI1NiAyMDIwIENBMTAeFw0yMjAxMTAwMDAwMDBa
    Fw0yMzAyMTAyMzU5NTlaMEwxCzAJBgNVBAYTAkZSMRAwDgYDVQQHEwdMQU5OSU9O
    MRMwEQYDVQQKEwpFS0lOT1BTIFNBMRYwFAYDVQQDDA0qLmVraW5vcHMuY29tMIIB
    IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmc4SzfLPgNR34dpaYWuJLUgA
    xwOmQ0zLRWy9o1skm3wBemGSXawhLv3yOwh3Z/6Z0g+N7jEJXX7sdYJAcffAhvJV
    0obHEXXIbeZviFDdHElUiRPy/wwpIcpatP1RYbvvZxEKNHszIKxSEZrNbFVKWRmK
    5ow2hb1B/WOtXrbROu9WtBAVOkgv8hfeTTaWtnk0LUUr2iXuqeg2o4+nJGgXAISi
    MRm8Mf/DyfysCyqQRgmYHM5cicm1AOosG0eh2QgUMypD8bB3wnpFePDJSYu3eDEL
    70cpGSzVzBwaRo66viHqcErgD/mRJc+H1uKJaWnUdynbKdp8TqWanF6d+ZAi3QID
    AQABo4IDkjCCA44wHwYDVR0jBBgwFoAUt2ui6qiqhIx56rTaD5iyxZV2ufQwHQYD
    VR0OBBYEFME5kYA9e1NOK1uiEECnHslqsRLNMD0GA1UdEQQ2MDSCDSouZWtpbm9w
    cy5jb22CFmdpdGxhYi1leHQuZWtpbm9wcy5jb22CC2VraW5vcHMuY29tMA4GA1Ud
    DwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwgY8GA1Ud
    HwSBhzCBhDBAoD6gPIY6aHR0cDovL2NybDMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0
    VExTUlNBU0hBMjU2MjAyMENBMS0yLmNybDBAoD6gPIY6aHR0cDovL2NybDQuZGln
    aWNlcnQuY29tL0RpZ2lDZXJ0VExTUlNBU0hBMjU2MjAyMENBMS0yLmNybDA+BgNV
    HSAENzA1MDMGBmeBDAECAjApMCcGCCsGAQUFBwIBFhtodHRwOi8vd3d3LmRpZ2lj
    ZXJ0LmNvbS9DUFMwfQYIKwYBBQUHAQEEcTBvMCQGCCsGAQUFBzABhhhodHRwOi8v
    b2NzcC5kaWdpY2VydC5jb20wRwYIKwYBBQUHMAKGO2h0dHA6Ly9jYWNlcnRzLmRp
    Z2ljZXJ0LmNvbS9EaWdpQ2VydFRMU1JTQVNIQTI1NjIwMjBDQTEuY3J0MAwGA1Ud
    EwEB/wQCMAAwggF9BgorBgEEAdZ5AgQCBIIBbQSCAWkBZwB2AOg+0No+9QY1MudX
    KLyJa8kD08vREWvs62nhd31tBr1uAAABfkQqaxUAAAQDAEcwRQIhAKC4KmXMJg6i
    CxXDC+4LuKRON0563dWCTtJJlRr4zfj2AiArt6e8+/0gydu4+PUkCshK2pYJicV7
    4MTxK00EFr7ETQB2ADXPGRu/sWxXvw+tTG1Cy7u2JyAmUeo/4SrvqAPDO9ZMAAAB
    fkQqa0YAAAQDAEcwRQIhAJl7pWRxpm0gbOg4zLjX/2kvA8gpcJweqVR/u83IkES+
    AiBSJfP5cE6uLOLtbq24xB/zpS0+dh8HTpcJN+9kQ3yuZgB1ALNzdwfhhFD4Y4bW
    BancEQlKeS2xZwwLh9zwAw55NqWaAAABfkQqa3UAAAQDAEYwRAIgJR17r8C4yYUd
    mozvcz3IuNZ5NOkEEzWGURk1BK7YiFICIGpJ4/Zb2qTmdA0qZSp3ckoqF0ZvmhCz
    cDIFfXK7Lo6tMA0GCSqGSIb3DQEBCwUAA4IBAQC+PrtHa/D4WOZQipW6aqUpj8JK
    rzuI32KL9bE1GoN474xfFnC+b980EMUE8ZcacEDMeOLa4jyMpDA3K70ADpNOpK9q
    D8bCA8J4n1rsHSIMXH44EegE0+2GoqfgJ7WokQfOqKB/Bxyi2+xw+Ix3kHC2nMn7
    kW0Xwu2Q2QZj2kXDw+uQG4eQk/Do0PpgHDPXkZlpW8l0h06oJX2Moa9r/rSNQmBV
    uyO/KP4r83gPtcfB+Mm50gxWiExPYL7g+I1EzhXHyf2m8/O1/liezOXKmtmIDBvz
    p36KBzDs/c8Qw5vxOmldSOTbqd2XaQxfs+tFfjOQSaOFKMUG3SlIoGHnd3G8
    -----END CERTIFICATE-----
    1 s:/C=US/O=DigiCert Inc/CN=DigiCert TLS RSA SHA256 2020 CA1
    i:/C=US/O=DigiCert Inc/OU=www.digicert.com/CN=DigiCert Global Root CA
    -----BEGIN CERTIFICATE-----
    MIIE6jCCA9KgAwIBAgIQCjUI1VwpKwF9+K1lwA/35DANBgkqhkiG9w0BAQsFADBh
    MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
    d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD
    QTAeFw0yMDA5MjQwMDAwMDBaFw0zMDA5MjMyMzU5NTlaME8xCzAJBgNVBAYTAlVT
    MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxKTAnBgNVBAMTIERpZ2lDZXJ0IFRMUyBS
    U0EgU0hBMjU2IDIwMjAgQ0ExMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
    AQEAwUuzZUdwvN1PWNvsnO3DZuUfMRNUrUpmRh8sCuxkB+Uu3Ny5CiDt3+PE0J6a
    qXodgojlEVbbHp9YwlHnLDQNLtKS4VbL8Xlfs7uHyiUDe5pSQWYQYE9XE0nw6Ddn
    g9/n00tnTCJRpt8OmRDtV1F0JuJ9x8piLhMbfyOIJVNvwTRYAIuE//i+p1hJInuW
    raKImxW8oHzf6VGo1bDtN+I2tIJLYrVJmuzHZ9bjPvXj1hJeRPG/cUJ9WIQDgLGB
    Afr5yjK7tI4nhyfFK3TUqNaX3sNk+crOU6JWvHgXjkkDKa77SU+kFbnO8lwZV21r
    eacroicgE7XQPUDTITAHk+qZ9QIDAQABo4IBrjCCAaowHQYDVR0OBBYEFLdrouqo
    qoSMeeq02g+YssWVdrn0MB8GA1UdIwQYMBaAFAPeUDVW0Uy7ZvCj4hsbw5eyPdFV
    MA4GA1UdDwEB/wQEAwIBhjAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIw
    EgYDVR0TAQH/BAgwBgEB/wIBADB2BggrBgEFBQcBAQRqMGgwJAYIKwYBBQUHMAGG
    GGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTBABggrBgEFBQcwAoY0aHR0cDovL2Nh
    Y2VydHMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0R2xvYmFsUm9vdENBLmNydDB7BgNV
    HR8EdDByMDegNaAzhjFodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vRGlnaUNlcnRH
    bG9iYWxSb290Q0EuY3JsMDegNaAzhjFodHRwOi8vY3JsNC5kaWdpY2VydC5jb20v
    RGlnaUNlcnRHbG9iYWxSb290Q0EuY3JsMDAGA1UdIAQpMCcwBwYFZ4EMAQEwCAYG
    Z4EMAQIBMAgGBmeBDAECAjAIBgZngQwBAgMwDQYJKoZIhvcNAQELBQADggEBAHer
    t3onPa679n/gWlbJhKrKW3EX3SJH/E6f7tDBpATho+vFScH90cnfjK+URSxGKqNj
    OSD5nkoklEHIqdninFQFBstcHL4AGw+oWv8Zu2XHFq8hVt1hBcnpj5h232sb0HIM
    ULkwKXq/YFkQZhM6LawVEWwtIwwCPgU7/uWhnOKK24fXSuhe50gG66sSmvKvhMNb
    g0qZgYOrAKHKCjxMoiWJKiKnpPMzTFuMLhoClw+dj20tlQj7T9rxkTgl4ZxuYRiH
    as6xuwAwapu3r9rxxZf+ingkquqTgLozZXq8oXfpf2kUCwA/d5KxTVtzhwoT0JzI
    8ks5T1KESaZMkE4f97Q=
    -----END CERTIFICATE-----
    2 s:/C=US/O=DigiCert Inc/OU=www.digicert.com/CN=DigiCert Global Root CA
    i:/C=US/O=DigiCert Inc/OU=www.digicert.com/CN=DigiCert Global Root CA
    -----BEGIN CERTIFICATE-----
    MIIDrzCCApegAwIBAgIQCDvgVpBCRrGhdWrJWZHHSjANBgkqhkiG9w0BAQUFADBh
    MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
    d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBD
    QTAeFw0wNjExMTAwMDAwMDBaFw0zMTExMTAwMDAwMDBaMGExCzAJBgNVBAYTAlVT
    MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j
    b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMIIBIjANBgkqhkiG
    9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4jvhEXLeqKTTo1eqUKKPC3eQyaKl7hLOllsB
    CSDMAZOnTjC3U/dDxGkAV53ijSLdhwZAAIEJzs4bg7/fzTtxRuLWZscFs3YnFo97
    nh6Vfe63SKMI2tavegw5BmV/Sl0fvBf4q77uKNd0f3p4mVmFaG5cIzJLv07A6Fpt
    43C/dxC//AH2hdmoRBBYMql1GNXRor5H4idq9Joz+EkIYIvUX7Q6hL+hqkpMfT7P
    T19sdl6gSzeRntwi5m3OFBqOasv+zbMUZBfHWymeMr/y7vrTC0LUq7dBMtoM1O/4
    gdW7jVg/tRvoSSiicNoxBN33shbyTApOB6jtSj1etX+jkMOvJwIDAQABo2MwYTAO
    BgNVHQ8BAf8EBAMCAYYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUA95QNVbR
    TLtm8KPiGxvDl7I90VUwHwYDVR0jBBgwFoAUA95QNVbRTLtm8KPiGxvDl7I90VUw
    DQYJKoZIhvcNAQEFBQADggEBAMucN6pIExIK+t1EnE9SsPTfrgT1eXkIoyQY/Esr
    hMAtudXH/vTBH1jLuG2cenTnmCmrEbXjcKChzUyImZOMkXDiqw8cvpOp/2PV5Adg
    06O/nVsJ8dWO41P0jmP6P6fbtGbfYmbW0W5BjfIttep3Sp+dWOIrWcBAI+0tKIJF
    PnlUkiaY4IBIqDfv8NZ5YBberOgOzW6sRBc4L0na4UU+Krk2U886UAb3LujEV0ls
    YSEY1QSteDwsOoBrp+uvFRTp2InBuThs4pFsiv9kuXclVzDAGySj4dzp30d8tbQk
    CAUw7C29C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=
    -----END CERTIFICATE-----


    Client certificate chain :

    -----BEGIN CERTIFICATE-----
    MIIFiDCCA3CgAwIBAgICEAAwDQYJKoZIhvcNAQELBQAwTzELMAkGA1UEBhMCRlIx
    EDAOBgNVBAoMB0VraW5vcHMxEzARBgNVBAsMCkVraW5vcHMgU0ExGTAXBgNVBAMM
    EFJvb3QgQ0EgSU5GUkEwMDEwHhcNMjIwMTE4MTg1NTE3WhcNNDcwMTEyMTg1NTE3
    WjBXMQswCQYDVQQGEwJGUjEQMA4GA1UECgwHRWtpbm9wczETMBEGA1UECwwKRWtp
    bm9wcyBTQTEhMB8GA1UEAwwYSW50ZXJtZWRpYXRlIENBIElORlJBMDAxMIICIjAN
    BgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAuKPkgtrebeQZBYMI/mcVFoSDSkAQ
    oAW1Pkkv20LfIM/gprDpGKmLnAsM8yxxWeiIiJl/5/kSlACz5Yt6YHIRLgImE/fD
    cHhOf0tWhI/iSv8bwB/PKUEhdEkkcxdhm87P3hCrU7oGchZdkughD6tBCD74B8n2
    LDA675dP2DeNg5a97U10Wa9b4tyVGGhaZCS7FWuvpAth8wwBN0pygDQWPi1rBQ0G
    DGRXVDvMgXw61NPsVOepIgfb6TLAWkqjsmarYvsHWClqWLGy5JPR/bEjWarDOZ5P
    xx5bVWWP/elD1wrrXA38jkgEmlNFJNycaXX9G9E54V9+xPVZxOCvILQSdtbymAFA
    J3abaCMx7IbMxVWEKanr0rScZz05jM6D9jb2y+M55umBjRCRu0WyA9qyo+BejS02
    OYeUHBE1tUqs3m6mC9Lhoiq1YBDT70zISYJb7jnl3Lu/bsSxl++ROHbmfiFT/aVX
    eaH1MOvEcG4CTXzvuRdEIvW5U3XD0MM7DRa5tY68mi39eYBhpvoXkcaR56bxPjGB
    MVXKDSoEMUqwubLA1uGL4CCQ57RtdX0Mi0o6VCdxALt/7UT8riuiD0JLoINf+Yq1
    nsOHakqLHkpr1SWWMfGq6YsX7xeRme/RINh9tdVhgDyAyWBglxnlHd+Yfz+v4pfD
    rx/b6Kxl0bLL0yUCAwEAAaNmMGQwHQYDVR0OBBYEFDFKQgIeG8JcyagKJG1+kyRv
    5mnLMB8GA1UdIwQYMBaAFBW8n1TXR/zisAjpwcZ+oHsinRsnMBIGA1UdEwEB/wQI
    MAYBAf8CAQAwDgYDVR0PAQH/BAQDAgGGMA0GCSqGSIb3DQEBCwUAA4ICAQAcWdy5
    Q6XhcTBMSxCYTdOnutGIO9U5o3T+72xN5RzL4xRfWX0jNlp+RIIUnmAtr2uYad87
    We2VfDD4lQOudxUOUyF48wvcFFBjJyGoU73FDPmTOxBGnE2jP8a8++K7olt2e5PK
    5vaOQPKpHaJTaFkx2ZmeyymrFAXFgQ4G/VsszzY7Lj35kNd1Cv2h5d02zPYP/A3T
    5FkO73ZRsXQP+l/bUJg+8Kgtd/bKRCYYOAQONOewzd68Bz9sesaqzQuOKWQZsy/q
    nE2/gmCAsaakxbUfFDTafE3rnqCKnl1oFviBh/Z3wArSNBRZPxS2rjiLVrDR8aCk
    agvZ2BGlUKEvG8H246Z+IyH7Ci+kVjCmRca1MH7LhMfpCkzIiTUaE0w3rezjsAIQ
    WMkoBZaioFCrlIBgXqlfHjeLmQ6sO1HDdhM5wOeCi8UJPXwzPKz/I6SOn4RJI05+
    b/GspJCB3++0jxQwbTJ8Pem4LmT1ILv6JhWF5Dlhzuc9tuaV6VXVX7801/rtzPAC
    T8NjGf+uJ4N/wsbyMCom3AGH9/fKKMB7YZ1KJ4AnqhvndRmnEhJB7lPkt4dfRju9
    66m2TzE2vjSk3w3JSrAOWIvOnAmr6uOPrFqLsyqOIkr6h8iv//wswoGdJd2eetni
    sX39GlxX26JpB4BzuQeB1ON6iH6mv6phZr6HzA==
    -----END CERTIFICATE-----
    -----BEGIN CERTIFICATE-----
    MIIFhDCCA2ygAwIBAgIJAMc264yrl32DMA0GCSqGSIb3DQEBCwUAME8xCzAJBgNV
    BAYTAkZSMRAwDgYDVQQKDAdFa2lub3BzMRMwEQYDVQQLDApFa2lub3BzIFNBMRkw
    FwYDVQQDDBBSb290IENBIElORlJBMDAxMB4XDTIyMDExODE4NTUwOVoXDTQ3MDEx
    MjE4NTUwOVowTzELMAkGA1UEBhMCRlIxEDAOBgNVBAoMB0VraW5vcHMxEzARBgNV
    BAsMCkVraW5vcHMgU0ExGTAXBgNVBAMMEFJvb3QgQ0EgSU5GUkEwMDEwggIiMA0G
    CSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCppigchqche4p2EZCtHEDP3nHpXfu/
    UxNCtNkE2MKSvQgzBSHzor0Mg0GqRqtHRZbJmbgXE09eFPOdNuRipQYw5KxmNPL1
    rDJQW+z9ju7q3LRN2p4VKLnAi1SeyoFPCEHWoGlX8rYmVZS75dhaBmTmgsSJY9qU
    1zetlqIf1CkTIVJblzY8meAX9zJUj/plSQv7vsSe10eTJ9VIRWUHDnDUkyCtc7rY
    VT7QDkFG+JTmMqqIKYn+Fr+hACC6KQnsSKHnv5g2su4LxwJZRx91WmNZ8i+6TWZN
    6Uujr74P38Rskle0+ZqK8GJRNrXexlFa6nqI0GbWUjWVKYXNJtKpQmHwnmxD10/1
    G73QOme077t3d6TeNePD9N3xfIkkl805rffykPBnF42kMyVsfbGTNYJ7cN+WwQSQ
    ZKe9qfSVzG8MpToo1P2HsLkgYJjQKOnOQh/pgbE71n2s58lYEnw6y73LMgCJgTUi
    QRJFeKVB4TNrJHHpKqXUpgM9JAQ6TX9lRC2TBYEsKag/csqrFDiQ4/5jmzJJli+k
    ZypUswWashOS3xZgQAyrBpyK27TFoA7t+p6mVei/M9T58aXWZ+aYiNsjixbsWbxY
    EBm+6IHdopLQot4NhbgxxwLUGfOVqJ1gB2CPS5qAi2qe4k2OkINHJAI5GUnTnuoX
    EkJlWY3GIShUaQIDAQABo2MwYTAdBgNVHQ4EFgQUFbyfVNdH/OKwCOnBxn6geyKd
    GycwHwYDVR0jBBgwFoAUFbyfVNdH/OKwCOnBxn6geyKdGycwDwYDVR0TAQH/BAUw
    AwEB/zAOBgNVHQ8BAf8EBAMCAYYwDQYJKoZIhvcNAQELBQADggIBAKDXcmAgc6l3
    km/mHfUT3sDsIASPNWV+Ll8k0nqtHZLXEUrs8B8U9XHwHJojEg/HEKU2fRbIhsMq
    bSwWSYdxMd3y3v5XoCQ1jcrbdJV82G7rnnOLsbc4H7hsdMzhkJxU01Ya7MZpIqNV
    T6p5OSAfbrWnET+k8igDkWucoZ4vUge3yo6Xbruwd/9wjFhvk9iRtZJ+AOcM67K3
    s/LfxzV/vE8MIyWhozAlvsGoR4EMQSxVGOUFoemlluw5IeXMyBJigKLmEJkWTB7Y
    8/xhbdE5qwQoD2xl1zbDCK+0325hs2jO15I4jWNutcmagkOcW/iVypCpMgi+KNIX
    aynqggvRWPQOsOjC2NBByaoxVSvvHkP66TO3Y/xP0EHdTmH0/RRhChO7ZGtUI4pg
    wOqLKOOOp/k1aYFEUAkVmUlEBYvMvWXUZJUIaYhO8X4sd3MpU8qRz9TduqLgzmol
    luIsOZfcFlc7fKu3zMMgXelZRbsuyGckouUfxGIN1IdtCIqmWPjmcpxOmeMp3731
    MfdgF5vJSr1uJ8Csoms8WHmmYKr7zvuzH7nQ7ynLjYbiTOZplpx/xAacdysbXW5Q
    eEb2GUHeCeKOh4ObagnZD1Q7gfYzfnZVNakQV4yR0f9N3R99KeDTi/+eDU2ow09G
    TXfx7j6SJFatsnNtLSwck+KkKwnOPMiQ
    -----END CERTIFICATE-----


Access control
==============

Customer portal integration
---------------------------

No customer portal integration for this phase. External users are managed by administrator roles directly in Gitlab, and stored in Gitlab database.


User authentication
-------------------

Ekinops logins and credentials are used for GUI access of Ekinops internal user ( Ekinops AD). Project token can be created by DevSecOps team leaders in order to publish artefact using the API.

As there is not yet a customer portal integration, external users are managed manually by adding new local user to gitlab. Then external users can connect to the gitlab GUI and  create tokens to pull artefact using the API.

Note that both users type need on top of the logging mechanism an Ekinops signed certificate ( see MTLS section )


Logging of activity
===================

/var/log/gitlab/gitlab-rails/api_json.log logs all queries.



.. code-block:: json
    :emphasize-lines: 8,11,15

    {
    "time": "2022-01-06T11:06:09.729Z",
    "severity": "INFO",
    "duration_s": 0.3456,
    "db_duration_s": 0.08693,
    "view_duration_s": 0.25867,
    "status": 404,
    "method": "GET",
    "path": "/api/v4/projects/2/packages/generic/my_package/0.0.1/onemanage_1.0-1_amd64.deb",
    "params": [],
    "host": "gitlab-ext.ekinops.com",
    "remote_ip": "10.1.133.53, 127.0.0.1",
    "ua": "curl/7.74.0",
    "route": "/api/:version/projects/:id/packages/generic/:package_name/*package_version/:file_name",
    "user_id": 7,
    "username": "external.user",
    "queue_duration_s": 0.015566,
    "redis_calls": 1,
    "redis_duration_s": 0.005777,
    "redis_write_bytes": 53,
    "redis_shared_state_calls": 1,
    "redis_shared_state_duration_s": 0.005777,
    "redis_shared_state_write_bytes": 53,
    "db_count": 5,
    "db_write_count": 0,
    "db_cached_count": 0,
    "db_replica_count": 0,
    "db_primary_count": 5,
    "db_replica_cached_count": 0,
    "db_primary_cached_count": 0,
    "db_replica_wal_count": 0,
    "db_primary_wal_count": 0,
    "db_replica_wal_cached_count": 0,
    "db_primary_wal_cached_count": 0,
    "db_replica_duration_s": 0,
    "db_primary_duration_s": 0.015,
    "cpu_s": 0.272031,
    "mem_objects": 62926,
    "mem_bytes": 14918263,
    "mem_mallocs": 63675,
    "mem_total_bytes": 17435303,
    "pid": 256667,
    "correlation_id": "01FRQH2JJP4P103CPE4NJYNFXG",
    "meta.user": "external.user",
    "meta.caller_id": "GET /api/:version/projects/:id/packages/generic/:package_name/*package_version/:file_name",
    "meta.remote_ip": "10.1.133.53",
    "meta.feature_category": "package_registry",
    "meta.client_id": "user/7",
    "request_urgency": "default",
    "target_duration_s": 1
    }
    

So by filtering we can get the **username**, the *remote_ip* and the **path** containing the package name.

You also have the access through GUI in  /var/log/gitlab/gitlab-tails/production_json.log

.. code-block:: json

    {
    "method": "GET",
    "path": "/test-package-repository/test-packages/-/package_files/8/download",
    "format": "html",
    "controller": "Projects::Packages::PackageFilesController",
    "action": "download",
    "status": 200,
    "time": "2022-01-12T18:12:24.853Z",
    "params": [
        {
        "key": "namespace_id",
        "value": "test-package-repository"
        },
        {
        "key": "project_id",
        "value": "test-packages"
        },
        {
        "key": "id",
        "value": "8"
        }
    ],
    "correlation_id": "01FS7QVCGCT89RF5MKKMAQ58Y0",
    "meta.user": "didier.lesage",
    "meta.project": "test-package-repository/test-packages",
    "meta.root_namespace": "test-package-repository",
    "meta.caller_id": "Projects::Packages::PackageFilesController#download",
    "meta.remote_ip": "90.0.201.35",
    "meta.feature_category": "package_registry",
    "meta.client_id": "user/3",
    "remote_ip": "90.0.201.35",
    "user_id": 3,
    "username": "didier.lesage",
    "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36",
    "queue_duration_s": 0.010326,
    "request_urgency": "default",
    "target_duration_s": 1,
    "redis_calls": 2,
    "redis_duration_s": 0.000427,
    "redis_read_bytes": 183,
    "redis_write_bytes": 1207,
    "redis_shared_state_calls": 1,
    "redis_shared_state_duration_s": 0.000174,
    "redis_shared_state_write_bytes": 53,
    "redis_sessions_calls": 1,
    "redis_sessions_duration_s": 0.000253,
    "redis_sessions_read_bytes": 183,
    "redis_sessions_write_bytes": 1154,
    "db_count": 10,
    "db_write_count": 0,
    "db_cached_count": 3,
    "db_replica_count": 0,
    "db_primary_count": 10,
    "db_replica_cached_count": 0,
    "db_primary_cached_count": 3,
    "db_replica_wal_count": 0,
    "db_primary_wal_count": 0,
    "db_replica_wal_cached_count": 0,
    "db_primary_wal_cached_count": 0,
    "db_replica_duration_s": 0,
    "db_primary_duration_s": 0.006,
    "cpu_s": 0.071422,
    "mem_objects": 25297,
    "mem_bytes": 3733616,
    "mem_mallocs": 9967,
    "mem_total_bytes": 4745496,
    "pid": 1463297,
    "db_duration_s": 0.02227,
    "view_duration_s": 0,
    "duration_s": 0.10937
    }


and in /var/log/gitlab/gitlab-workhorse/current with the same correlation_id, we can find the name of the package :

.. code-block:: json

    {
    "correlation_id": "01FS7QVCGCT89RF5MKKMAQ58Y0",
    "file": "/var/opt/gitlab/gitlab-rails/shared/packages/4e/07/4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce/packages/2/files/8/onemanage_1.0-1_amd64.deb",
    "level": "info",
    "method": "GET",
    "msg": "Send file",
    "time": "2022-01-12T19:12:24+01:00",
    "uri": "/test-package-repository/test-packages/-/package_files/8/download"
    } 


.. warning::

    The "current" file as an history of 30 files of 24 hours or 200MB

    Other files have an history of 30 files of 1 day.

    Need to be increased to 1 year.


.. note::

    Those logs could be sent to a central system in a syslog format. Ekinops does not have yet this kind of infrastructure.

Business continuity (BCP)
*************************


Resilience and High Availability
================================

No system in plave for High Availability

Backup plan
===========

Daily incremental backup and a weekly full backup with an history of ???? days  ( Stijn ??)

.. warning::
    Need 1 year of backup. Could we keep a backup per month, and last 11 of those backups ?



Operations
**********


Users management
================

Ekinops logins and credentials are used for GUI access of Ekinops internal user (Ekinops AD). Project token can be created by DevSecOps team leaders in order to publish artefact using the API.

As there is not yet a customer portal integration, external users are managed manually by adding new local user to gitlab. Then external users can connect to the gilan  GUI and  create tokens to pull artefact using the API.

Note that both users type need on top of the logging mechanism an Ekinops signed certificate ( see MTLS section )


Ekinops policy regarding external user is :

  - give only read access to a selected set of repositories
  - manage their password and tokens


For that, external user must be created with those default parameters :

- remove "can create group"

- use "Access level" : **Regular**

- validate "**External**"

- do **not** validate "Validate user account"


Then assign the new user to projects containing the packages he is allowed to access and put is role to **"Reporter"**

With this role, the user will be in read mode for the actions he can do. See https://docs.gitlab.com/ee/user/permissions.html for the details.

External users:

- Can only create projects (including forks), subgroups, and snippets within the top-level group to which they belong. ( Not possible in this instance)
- Can only access public projects and projects to which they are explicitly granted access, thus hiding all other internal or private ones from them (like being logged out).
- Can only access public groups and groups to which they are explicitly granted access, thus hiding all other internal or private ones from them (like being logged out).
- Can only access public snippets.

and with reporter role, an external user can: 

- view analytics
- view security analysis results
- Download and browse CI/CD Job artefacts
- View CI/CD information
- Pull from container registry (if permitted in registry rules)
- View Gitlab pages (not activated on projects)
- View and create incidents (not activated on projects)
- View and create Issues (not activated on projects)
- View license compliancy information (not activated on projects)
- Assign a reviewer to merge requests 
- View metrics dashboards
- Pull a package from package registry
- Download a project, pull project code


Repositories and flow of artefacts
==================================

A repository is linked to a project. All artefacts in a project have same access right. External users who need to access a specific repository is added to the project owning the specific repository

Supported type of artefacts 

- raw artefacts  ( max size : 5 368 709 120 bytes)

Three groups are created with **Private** visibility

- SDx-Cloud

- Transport

- Access

Only internal user (main giltab CI token) should be added to those groups to be able to push packages to all projects under those groups.

External users are individualy added to each project as **Reporter** role depending on their access rights

Repositories are created by the creation of the projects. **Private** visibility level is chosen. A git repository is initialized with a README file containing useful information for the external users.

Settings of project must be changed in General: check that Visibility is **Private**, remove "Users can request access",  change Issues to **deactivated**, 




Artefacts security
==================

Gitlab does not manage signature and signature checking of raw artefact. Signature can be managed as an additional file stored as a raw artefact

Security scanning : no scan done in this server.



User documentation (external user)
**********************************

Customer documentation is published as a README.md file of the gitlab project. This file also contain specific information of the associated artefact repository.

Here is the content:


How to retrieve packages
========================

Package management is done through Gitlab API:
https://docs.gitlab.com/ee/api/packages.html

We will detail below how to retrieve packages from current repository,
using cUrl to call the API. All commands are supposed to be executed on
a Linux distribution with cUrl installed.

The user is supposed to already have: 

- a **certificate** signed by Ekinops to authenticate to Gitlab external instance (consisting in a private key, and the public certificate signed) 
- a **Gitlab API token** with enough rights to pull packages and not pushing/removing them. Go to https://gitlab-ext.ekinops.com/-/profile/personal\_access\_tokens in order to generate it, with read\_user and read\_api rights.


Variables definition
--------------------

First, the user has to provide the following variables: 

- ``CERT_KEY_PATH`` : the path where the certificate key used to authenticate to Gitlab external instance is stored 
- ``CERT_FILE_PATH`` : the path where the certificate file used to authenticate to Gitlab external instance is stored 
- ``EXTERNAL_USER_TOKEN`` : The external user API token, with enough rights to pull packages and not pushing/removing them. 
- ``PROJECT_ID`` : the current project ID (that we can find on the main project page, like https://gitlab-ext.ekinops.com/sdx-cloud/onemanage) 
- ``PACKAGE_NAME`` : the package name, which will be on top of the package file path (:math:`PACKAGE_NAME/`\ PACKAGE\_VERSION/$PACKAGE\_FILE)
- ``PACKAGE_VERSION`` : the package version 
- ``PACKAGE_FILE`` : the package file name (a package version can containe multiple files)


::

    CERT_KEY_PATH=xx/xx.key
    CERT_FILE_PATH=xx/xx.pem
    EXTERNAL_USER_TOKEN=xxxxxxxxx
    PROJECT_ID=4   # Place the project ID here
    PACKAGE_NAME=XXXXX
    PACKAGE_VERSION=X.X.X
    PACKAGE_FILE=xxxxx.xxx

Download package with external user token
-----------------------------------------

::

    curl --key $CERT_KEY_PATH --cert $CERT_FILE_PATH --header "PRIVATE-TOKEN: $EXTERNAL_USER_TOKEN" \
    --output $PACKAGE_FILE https://gitlab-ext.ekinops.com/api/v4/projects/${PROJECT_ID}/packages/generic/$PACKAGE_NAME/$PACKAGE_VERSION/$PACKAGE_FILE

Then, the file will be output locally under ``$PACKAGE_FILE`` name as
defined with ``--output`` option.

List all packages from the project
----------------------------------

::

    curl --key $CERT_KEY_PATH --cert $CERT_FILE_PATH --header "PRIVATE-TOKEN: $EXTERNAL_USER_TOKEN" \
    "https://gitlab-ext.ekinops.com/api/v4/projects/${PROJECT_ID}/packages"

This command will provide the following json output. It is then possible
to extract the **package ID** that we will use later.

::

    [
      {
        "id": 1,
        "name": "com/mycompany/my-app",
        "version": "1.0-SNAPSHOT",
        "package_type": "maven",
        "created_at": "2019-11-27T03:37:38.711Z"
      },
      {
        "id": 2,
        "name": "@foo/bar",
        "version": "1.0.3",
        "package_type": "npm",
        "created_at": "2019-11-27T03:37:38.711Z"
      },
      {
        "id": 3,
        "name": "Hello/0.1@mycompany/stable",
        "conan_package_name": "Hello",
        "version": "0.1",
        "package_type": "conan",
        "_links": {
          "web_path": "/foo/bar/-/packages/3",
          "delete_api_path": "https://gitlab.example.com/api/v4/projects/1/packages/3"
        },
        "created_at": "2029-12-16T20:33:34.316Z",
        "tags": []
      }
    ]

List all package files from a single package
--------------------------------------------

Here, we have to define the **package ID** that we could find from the
previous command and store under the ``PACKAGE_ID`` variable.

::

    PACKAGE_ID=X

    curl --key $CERT_KEY_PATH --cert $CERT_FILE_PATH --header "PRIVATE-TOKEN: $EXTERNAL_USER_TOKEN" \
    "https://gitlab-ext.ekinops.com/api/v4/projects/${PROJECT_ID}/packages/${PACKAGE_ID}/package_files"


User documentation (Ekinops user)
*********************************

Ekinops documentation is published here : (ekipedia page)

Ekinops user have additional authorization to push a package.


How to push packages
====================

Package management is done through Gitlab API:
https://docs.gitlab.com/ee/api/packages.html

We will detail below how to retrieve packages from current repository,
using cUrl to call the API. All commands are supposed to be executed on
a Linux distribution with cUrl installed.

The user is supposed to already have: 

- a **certificate** signed by Ekinops to authenticate to Gitlab external instance (consisting in a private key, and the public certificate signed) 
- a **Gitlab API token** with enough rights to pull packages and not pushing/removing them. Go to https://gitlab-ext.ekinops.com/-/profile/personal\_access\_tokens in order to generate it, with read\_user and read\_api rights.


Variables definition
--------------------

First, the user has to provide the following variables: 

- ``CERT_KEY_PATH`` : the path where the certificate key used to authenticate to Gitlab external instance is stored.
- ``CERT_FILE_PATH`` : the path where the certificate file used to authenticate to Gitlab external instance is stored.
- ``EXTERNAL_USER_TOKEN`` : The external user API token, with enough rights to pull packages and not pushing/removing them. 
- ``PROJECT_ID`` : the current project ID (that we can find on the main project page, like https://gitlab-ext.ekinops.com/sdx-cloud/onemanage
- ``PACKAGE_NAME`` : the package name, which will be on top of the package file path. (PACKAGE_NAME/PACKAGE_VERSION/PACKAGE_FILE).
- ``PACKAGE_VERSION`` : the package version.
- ``PACKAGE_FILE`` : the package file name. (a package version can contain multiple files)


::

    CERT_KEY_PATH=xx/xx.key
    CERT_FILE_PATH=xx/xx.pem
    EXTERNAL_USER_TOKEN=xxxxxxxxx
    PROJECT_ID=4   # Place the project ID here
    PACKAGE_NAME=XXXXX
    PACKAGE_VERSION=X.X.X
    PACKAGE_FILE=xxxxx.xxx

Download package with external user token
-----------------------------------------

::

    curl --key $CERT_KEY_PATH --cert $CERT_FILE_PATH --request PUT --header "PRIVATE-TOKEN: $EXTERNAL_USER_TOKEN" \
    --upload-file $PACKAGE_FILE https://gitlab-ext.ekinops.com/api/v4/projects/${PROJECT_ID}/packages/generic/$PACKAGE_NAME/$PACKAGE_VERSION/$PACKAGE_FILE



Cost model
**********

Each external user needs a seat. Gitlab seat is $220 per year.

Then an external user can create as many token he needs. Authorization of access to a specific project is based on a seat, not on tokens.



 


