RACI: Reponsibility matrix
**************************

With the DevSecOps infrastructure, we also promote shared responsibility between ICT, Infra and developers. This RACI matrix will define the responsibilities.

There are four types of relationships or roles in the RACI system:

- **the Responsible** – she or he takes care of the task, looking for a way to complete it on time and on budget
- **the Accountable** (Manager) – she or he  makes decisions and takes action for the tasks.
- **the Consulted** – she or he is involved and informed about decisions and tasks
- **Informed** – she or he will be informed about decisions and actions during the project

.. toctree::
   :maxdepth: 1
   
   GitlabGroupsRules
   internalartefactrepo
   externalartefactrepo
   SASTSonarqube
   

