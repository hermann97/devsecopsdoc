How to create or import new projects
====================================

Create new projects
*******************

Whenever you create a new empty project, you should always start with
an empty commit in order to be able to rebase the all the history
without any dependencies on the initial contents. It means that
the initial contents should rather be the 2nd commiet.

Import hg/oat projects
**********************

The following example shall convert a hg repository to a git one,

.. code-block:: console

  oat workspace create git
  cd git
  oat module clone wcf_ui
  cd wcf_ui
  cd ..
  git clone git://repo.or.cz/fast-export.git
  mkdir new_git_repository
  cd new_git_repository
  git init
  git config core.ignoreCase false
  git config --global user.name  "Vincent JARDIN"
  git config --global user.email  vincent.jardin@ekinops.com
  git config --global core.editor vi
  ../fast-export/hg-fast-export.sh -r ../wcf_ui
  git checkout main
  git remote add origin https://gitlab.ekinops.com/vincent.jardin/webui-oneos-webui.git
  git push -u origin --all
