R&D documentation
*****************

Introduction
============

At each phase of the process, R&D generates documentation. We start with a high-level design based on an analysis of product requirements. This documentation is linked to a product. The product is split into subsystems, each subsystem is split into modules of code stored in GitLab projects. For each GitLab project, documentation must be delivered or updated (low-level design, test campaign, results of static analysis, release note, ...). This documentation is linked to the sources and their corresponding DevSecOps workflow.

The goal is to link the documentation to the automatic workflow so pages can be autogenerated during pipeline execution. For documentation written by the developer (low-level design, description of unit tests, ...) source of this documentation is placed in the same repository as the code it belongs to, so writing documentation and code, reviewing of documentation and code are done at the same time.

Let's see in detail the different cases.

Internal documentation
======================


Internal Documentation linked with code workflow
------------------------------------------------

Here we are talking about documentation that is linked to the code or documentation automatically generated by the pipelines. In this case, documentation is written using the Restructured text format. It is stored as text files, with optional pictures in png, drawio, kroki format, for the documentation created or modified by the developer of the code. This documentation is built together with the code during the same pipeline phases. Then review can be done at the same time as code. A review server renders the documentation during the review process. At the release phase, the documentation is published automatically at the right place inside the R&D documentation server. The R&D documentation server is Ekipedia. Pages generated are read-only pages, but they can be mixed with other pages created within Ekipedia.

For autogenerated pages, templates of pages are stored in another GitLab project and can be used modified by the pipelines. Those pages can also contain JIRA JQL requests to create "active" pages on Ekipedia.

Here is the global workflow of those pages :


.. drawio-image:: graphics/internalcodedocflow.drawio

Internal Documentation Tree on Ekipedia
---------------------------------------

Internal R&D documentation, manually or automatically generated are organized following this structure:

.. drawio-image:: graphics/internaldocorg.drawio


External documentation
======================

External Documentation linked with code workflow
------------------------------------------------

External documentation linked with code workflow are mainly documentation generated during the release process that will be delivered together with the release. It can be the Open source licenses document, the release note, ...

Those documents are not published in Ekipedia as Ekipedia is for internal documentation. Those documents must be considered as artifacts line the release and must be delivered in the same way as the release, and so, pushed in an artifacts repository.

.. drawio-image:: graphics/externalcodedocflow.drawio


