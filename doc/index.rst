.. ModuleForNewDocumentation documentation master file, created by
   sphinx-quickstart on Fri Jul  2 11:28:50 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DevSecOps documentation
==================================

.. toctree::
   :maxdepth: 1

   EkinopsGitlabFlow
   RACI
   RDDocumentation
   HowTo
   GitlabCodeQuality
   GitlabUpgradeProcedures
   Elasticsearchintegration.rst



