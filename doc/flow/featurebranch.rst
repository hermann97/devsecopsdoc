Fork the project in your own space
==================================

Forking the project is a best practice as you can work without any impact on the original project. This is optional in our flow, but highly recommended. To fork the project, go the project page, and click on the *fork* button. Then un the next page, choose as namespace your username in the Project URL. Put visibility to *Internal*, to be able to share your work, then cklick on *Fork project*

.. figure:: ../images/fork_project.png
   :alt: gitlab fork project

Then, in order to keep the same membership as in the original project (especially to ask for merge request reviews and approvals), please provide the **maintainer role** to the AD group
which manages the original project.

To do so, you have to go to Project information -> Members menu from the left panel of your forked project page.

.. figure:: ../images/gitlab_project_membership.JPG
   :alt: gitlab project membership


Create feature branch
=====================
By being in the main branch with latest changes, use ``git checkout -b <branch_name>`` to create a feature branch.
To push the feature branch to remote use ``git push origin <branch name>`` command. 

For, each fix or feature implemention, a branch should be created.

Note that if you are not using the forking mode, then if multiple people are working on same project, you must take care do use different names for branches. 

It is not recommended to work together on same branch. It made the rebase operation complex.

Branch can also be created directly in gitlab. To create branch in gitlab, go to the project overview page, click on ``+`` symbol -> `New branch`, enter branch name,
select the base branch from where new branch needs to be created, then click create branch.
|

.. figure:: ../images/create_branch.png
   :alt: gitlab create branch

If you are using the webIDE of Gitlab, Gitlab will ask for a branch name when clicking on Commit, and will take care of the creation of the branch.


Guideline to follow for creation and maintenance of branch.

1. It is recommended that for each fix or feature implementation or any changes that needs to be made to main branch, a ticket should be created(if not present already).

2. The feature branch that is to be created should have the following format:

   <ticket-id> 

   <ticket-id>_<one word decription like fix,feature,integrate etc.> 

   <ticket-id>/<one word decription like fix,feature,integrate etc.> 

   e.g. PRT-666_fix, FR-111/fr, AGL-555_integ, FR-123 etc. 

3. Sometimes, multiple people can work on same module and same ticket, e.g. one developer working on cli part and one developer working on dataplane communication part. 

   The following format can be used for feature branch:

   <ticket-id>_<part of the feature that is fixed like cli,datamodel,data etc.> 

   <ticket-id>_<short username> 

   <ticket-id>/<part of the feature that is fixed like cli,datamodel,data etc.> 

   <ticket-id>/<short username> 

   e.g. PRT-666/sani, PRT-666_sani, PRT-666/cli, FR-111_datamodel, FR-111_fix_sani, FR-111_cli_sani etc. 

4. Pushing branches on original repository with branch name not following above format may get rejected. When using forking, the branch is in your own space, so you are free to choose any name.

5. Keep branch name short.

6. Keep local branches in sync with remote.

   Pushing regularly on remote is a way to save your work. It does not matter how many commit you do, as, before merging, you will clean the history.

   Deleting the branch in remote(e.g. after merged to main) will not delete the branch locally,
   For deleting the branch locally use ``git branch -D <branch name>`` command, 

   even then it will still show in git list branch(``git branch -a``) command. 
   
   To cleanup(sync with remote) local branches use ``git remote prune origin`` command.


