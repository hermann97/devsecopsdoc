Check Pipeline
==============
When a code is ready, a developer will push a code. This code will be automatically tested:

1. Static code analysis for formatting, bugs and security etc.
2. Build
3. L0, L1 tests (including security tests)
4. Optionally but recommended: some L2 and L3 tests dedicated to the feature or/and security testing, but must be fast.
5. Detection of existing CVE, check of license compliance, dependencies upgrade etc.

Each element of the pipeline generates a report in a common defined format. Those reports/results will be automatically regrouped in a review request, and when all is green, the developer can decide to assign the request to another, or several others developer for review. This pipeline is called the Check pipeline. Its goal is to help the developer to prepare the review request, assuring that the code is well formate, statically tested, dynamically unitary tested (L0, L1), and will  be integrated without regression ( L2, L3 tests ). 

In term of security, it is important to execute tests at this stage, but the duration of tests should remain very short in that stage.
