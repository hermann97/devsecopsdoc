Fast-Forward merge method
=========================
The difference between, merge commit and fast forward merge is that in fast-forward merge, git simply moves the source branch pointer to the target branch pointer without creating an extra merge commit.

And it can be performed when there is a direct linear path from the source branch to the target branch. However, a fast-forward merge is not possible if the branches have diverged. 


.. figure:: ../images/ff_merge.JPG
   :alt: Git fast forward

With fast-forward merge requests, you can retain a linear Git history and a way to accept merge requests without creating merge commits.

If we have multiple commits are ahead, since the branch was created, changes will have to be rebased in this case without rebase fast forward will not work.


.. note::
   `git rebase` rewrites the commit history. It can be harmful to do it in shared branches. It can cause complex and hard to resolve merge conflicts. In these cases, instead of rebasing your branch against the default branch, consider pulling it instead (`git pull origin master`). It has a similar effect without compromising the work of your contributors.

It is recomended to use fast-forward merge method. This will be default project setting.

