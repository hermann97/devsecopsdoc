.. _commit-change-ref-label:

Commit changes to feature branch
================================
For making changes available on the server so they can be saved and viewed by other users, changes are first staged using following commands: 

``git add <file>`` //For single file 

``git add --all`` //For all files that are changed 

After this the changes can be commited using command ``git commit -m "Commit message"``

Local committed changes can be pushed to remote using ``git push origin <branch_name>`` command.

Guidelines for committing changes:

1. Commit and push changes to the branch often, to make changes visible to others.
2. While pushing the changes to remote, if there is any unecessary commits and commit messages, edit and/or squash it(Clean up commit history)

   To do this interactive rebase can be used with command ``git rebase -i origin/<main branch name>``.

   Assume, branch FR-1234 is created and 5 commits are done but not pushed to remote.

.. figure:: ../images/interactive_rebase.JPG
   :alt: git log


   Now, if last 3 commits needs to be merged into single commit, using ``git rebase -i origin/develop`` command. Replace the `pick` below to `s` or `squash`, for commits which needs to be squashed.

.. figure:: ../images/interactive_rebase_squash_1.JPG
   :alt: git rebase


   change it to:

.. figure:: ../images/interactive_rebase_squash_2.JPG
   :alt: git interactive rebase


   Then the commit messages can be editted.

.. figure:: ../images/interactive_rebase_squash_3.JPG
   :alt: git interactive rebase


   The final commit log will look something like:

.. figure:: ../images/interactive_rebase_squash_4.JPG
   :alt: git log


   Now the changes can be pushed to remote.

.. note::
   Using 'git rebase -i origin/<main branch nam>' will also fetch the latest changes from main branch.
   There is however an option to rebase it against a specific commit id 'git rebase -i <sha id>'. This is particularly useful, when multiple people work on same branch one by one.


