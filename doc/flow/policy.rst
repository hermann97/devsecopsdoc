Merge request approval and commit policy
========================================
Approval rules
--------------

Commit policy
-------------

1. No commit to CI branch is allowed if the code/feature is not production ready. This may be automated where main and dependent changes can be merged in a single merge request and the commit hooks can directly update the version in CI branch when all the test/CD/CI requirements are met. Updating CI branch without feature being(ticket not updated) will cause problem for branching strategy(CI merge to main with photo tagging).
2. Clear description explaining the relevancy of the contribution. Proper issue tracking data should also be included.
3. Working and clean code that is commented where needed(Following code guidelines).
4. Unit, (integration, and system tests) that all pass on the CI server.
5. Regressions and bugs are covered with tests that reduce the risk of the issue happening again.
6. Performance guidelines have been followed.
7. No vulnerability is introduced
8. No compatibility break is introduced.
9. Documented in the /doc directory.
10. Changelog entry added, if necessary.
11. Reviewed by relevant reviewers and all concerns are addressed for Availability, Regressions, Security. Documentation reviews should take place as soon as possible, but they should not block a merge request.
12. Merged by a project maintainer.
13. Inform the Infra team when your contribution is changing default settings or introduces a new setting, if relevant. e.g. new automation script added for commit hooks.
14. Added to the release post, if relevant.
15. Relevant test logs, valgrind reports, profiling data, vulnerability report added if required(recommended) in merge request.
16. When creating feature/maintenance branch proper name requirements to be used.

