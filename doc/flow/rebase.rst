Rebase and commit cleanup
=========================
Before publishing the final merge request, or marking the draft merge request as ready, it is advisable to rebase the feature branch with main branch.

This is done to reduce the possibility of merge conflicts later as well as pull the changes of the main branch that happened after the creation of the feature branch.

If you are on a forked project, you need to rebase from the origial project (upstream). For that, you need to indicate to git where the upstream project is. 
Use ``git remote add upstream <url of original project>`` This url is the url of the upstream repository you forked from.

Then you can fetch changes from the upstream repository. It will not overwrite or modify your change in any way, it uploads the change in a local cache. 
If you are using a forked project do ``git fetch upstream``, but if you work on the original project, do ``git fetch origin``

Let's explain what rebasing is:

.. drawio-image:: ../graphics/ekiflow_rebase.drawio
  :format: png

The command used to rebase with the main branch is ``git rebase upstream/<main branch name>`` if you are using forked project, or ``git rebase origin/<main branch name>`` if you are working directly on riginal project.

To check that the rebase was successful, you can use ``git log --oneline --decorate --all --graph`` or any GUI tool like gitk, ...
Your commits should appear on top of upstream main latest commit, as shown on the previous picture.


When all is ok, you can push your branch with ``git push --force origin <yourbranchname>`` or better ``git push --force-with-lease origin <yourbranchname>`` as you will rewrite the history of your branch. Don't use the ``--force`` when working on main branch. You must not rewrite history on main branch, as this branch is shared between several persons. Please see https://docs.gitlab.com/ee/topics/git/git_rebase.html#regular-rebase for more details.


The git rebase and commit history cleanup can be done in a single step using interactive rebase mentioned here: :ref:`commit-change-ref-label`

When things go wrong, at any time you can abort a rebase by doing: ``git rebase --abort``.




