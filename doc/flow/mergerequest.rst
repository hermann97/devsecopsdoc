Creating merge requests
=======================
Merge request can be created in two ways.

It can be created when, pushing the feature/Dev branch to remote, use the link mentioned in the status message.

.. figure:: ../images/creating_merge_r.JPG
   :alt: Merge request


It can also, be created in `gitlab` after feature/dev branch is pushed to remote, by navigating to ``Merge Requests`` in main project menu. Click on ``New merge request`` -> ``select Source branch`` -> ``select Target branch`` -> ``Compare branches and continue``. Then fill in the details and then click ``Create merge request``

.. figure:: ../images/creating_merge_r2.JPG
   :alt: Merge request 2

Description template
--------------------
Sample merge request description template 

.. code-block:: none

   # R&D Summary
   < Description of the issue including jira ID, TAC id etc >
   # ROOT CAUSE
   < Description of what was causing problem including jira ID or TAC ID that introduced it(e.g. regression fix) >
   # FIX DESCRIPTION
   < Description of what fix was done >
   # SCOPE/IMPACT/RISK ANALYSIS
   < Description of how the fix will impact existing conditions like regression,compatibility,vulnerabilty,documentation change etc. >
   # DEPENDENCIES
   < Description of any dependency this merge request has on others >
   # MANUAL TEST COVERAGE
   < Description of unit tests done for testing the fix including data on profiling, memory leak analysis,test case ids that was used(internal or external like ipv6 ready/tahi etc.)etc. can point to any attachments supporting this >
   # AUTO TEST COVERAGE
   < Description of any automation/NRT/CIT tests run with links to the report(that are not covered in the check CI pipeline) >
   # KNOWN ISSUES/LIMITATION
   < Description of any known issues or limitation existing or introduced with this MR >
   # ADDITIONAL TEST NEEDED
   < Description of additional test advice for the testers/reporters >
   # ATTACHMENTS
   < Description of attachment added >

The merge request description template can be made permanent by copying the above to a `.md` file and place it under `.gitlab/merge_request_templates` directory of the project.

When creating merge request, select the template from drop-down list and edit it to add relevant/necessary information.

.. figure:: ../images/mr_desc.JPG
   :alt: Draft mr

Draft merge request
===================

When a part of the fix or feature is done, it is recommened that a draft merge request is raised for review, and whoever has the knowledge of the module are added as reviewer.

The reviewers may not be the owner/maintainer of the project.

Draft merge requests doesn't need an assignee.
Doing this will eliminate any design/implementational surprises that might come when review is done at last phase of delivery.

In gitlab flow, a draft merge request implies `work-in-progress`.

This can be done while creating merge requests. Edit the title to append it with ``Draft:``, this will automatically make the merge request as draft.

.. figure:: ../images/draft_mr.JPG
   :alt: Draft mr

Publish merge request changes
=============================
When a merge request is ready for final review and to be merged, a draft review should be marked as ready.
There are two ways to mark a draft review as ready.

1. Clicking the ``Mark as ready`` in the merge request page. This will also remove the `Draft:` prefix from the title.
2. Editing the merge request title to remove the `Draft:` prefix.

.. figure:: ../images/publish_draft.JPG
   :alt: Git publish draft

After merge request is marked as ready, assignee can be changed to whoever has the role of the maintainer(owner) for the project.

Only the merge request that is not in draft state are allowed to be merged.

