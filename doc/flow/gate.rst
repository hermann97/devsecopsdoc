Gate Pipeline
=============
Gate pipeline is triggered by the "go-to merge" of a code review. Goal is to merge. To do that, we must first check that the merging operation will not break something; perhaps the main trunk as evolved, some dependencies have changed etc. The gate pipeline will simulate the merge, do some non regression tests, and if all is ok, then merge will be done and pushed.