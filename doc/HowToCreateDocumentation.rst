How to create new documentation project
=======================================

  - :ref:`Purpose`
  - :ref:`Create new documentation project`
  - :ref:`Add new document file`
  - :ref:`Review of documentation`
  - :ref:`Publication of documentation`
  - :ref:`Publication on Ekipedia`

Purpose
*******

This document will explain how to create a new documentation project and how to complete the documentation

Create new documentation project
********************************

log in : https://gitlab.ekinops.com

To create a new documentation project you have to create a new project starting from template **EkiEmptyProject**

- Select **"Create new project"** in "Menu" (top/left)

.. image:: images/Gitlab_CreateNewProject.jpg

- Select **"Create from template"**

.. image:: images/Gitlab_CreateNewProject_2.jpg

- Select tab **"Instance"**. Then click on button **"Use template"** for **EkiEmptyProject**

.. image:: images/Gitlab_CreateNewProject_3.jpg

- Fill the **"Project name"**, the **"Project URL"** and the **"Visibility Level"**. When it is done, you can click on button "Create project"

.. image:: images/Gitlab_CreateNewProject_4.jpg


- Now you can clone your project on your local host using **"git clone"** command

For instance :

.. code-block:: bash

   git clone git@gitlab.ekinops.com:marc.michot/testcreationdocumentation.git

The URL can be copy from **CLONE** selection

.. image:: images/Gitlab_CreateNewProject_5.jpg


Add new document file
*********************

New branch
++++++++++

First you need to create a new branch from the main to add your documentation :

- Select **"Branches"** in left menu in "Repository / Branches"

.. image:: images/Gitlab_CreateNewFile.jpg

- Click on button **"New branch"**

.. image:: images/Gitlab_CreateNewFile_2.jpg

- Enter the **"Branch name"** and click on **"Create branch"** button

.. image:: images/Gitlab_CreateNewFile_3.jpg

**Your branch is now created**


New Merge Request
+++++++++++++++++

Then you need to create a merge request for your documentation

- Select **"Merge requests"** in left menu. Then click on **"New merge request"** button

.. image:: images/Gitlab_CreateNewFile_4.jpg

- Select the **"Source branch"** and the **"Target branch"**. Then click on **"Compare branches and continue"** button

.. image:: images/Gitlab_CreateNewFile_5.jpg

- Enter the **"Title"**, the **"assignees"** and the **"reviewers"**. WARNING you must enter at least **1** in **"Approvals required"** of **"Approval rules"**. Then click on **"Create merge request"** button

.. image:: images/Gitlab_CreateNewFile_6.jpg

Add new file
++++++++++++

And now you can add a documentation file under the **"doc"** directory...

To create **new documentation** you have to **CREATE** a **new markdown (.md) or reStructuredText (.rst) file** under directory "doc" of your project. But, integration of diagram is supported in .rst files only.

You can do it using **Gitlab portal** or using **git commands** to git server.


Add file using Gitlab GUI
-------------------------

- Select the Project (click on project name) (top/left)

- Select the **branch** to work into

.. image:: images/Gitlab_CreateNewFile_7.jpg

- Select the directory **doc**

- Select the directory **"New file"** in combo box **"+"**

.. image:: images/Gitlab_CreateNewFile_8.jpg

- Enter the file name and click on **"Commit changes"** button

.. image:: images/Gitlab_CreateNewFile_9.jpg

- Then you can edit the file using one of the two editors (Edit or Web IDE). WARNING : a documentation file **MUST** have a title (to be referenced)

.. image:: images/Gitlab_CreateNewFile_10.jpg

- Edit the **index.rst.template** file in **doc** directory.

.. image:: images/Gitlab_CreateNewFile_11.jpg

- First rename the edited file to **index.rst** (!!! IMPORTANT !!!) from the circled area on the top, add the name of the new file in the **toctree** session and click on **"Commit changes"** button

.. image:: images/Gitlab_CreateNewFile_12.jpg


Using git command
-----------------

- First you have to sync (update) your local project with remote on server

.. code-block:: bash

   cd testcreationdocumentation
   git pull

- Then you have to upgrade your project to the branch.

.. code-block:: bash

   git checkout newdocumentation

   
- Then you have to create a file under the **doc** directory. Then edit it.

.. code-block:: bash

   cd doc
   kate newdoc.rst

- Then you can push your file to the server

.. code-block:: bash

   cd ..
   git status
   git add .
   git commit -m "YOUR TEXT"
   git push


Edit new file
+++++++++++++

Then you have to **EDIT** the new file with information in **compatible text**. To do it you can use either one of the two editors in **Gitlab portal** or an external editor you like for markdown or reStructuredText. You will have to enter your documention in text respecting the markdown or reStructuredText syntax.

Therefore, you have to learn it :

- `markdown/basic-syntax <https://www.markdownguide.org/basic-syntax/>`_
- `reStructuredText/basic-syntax <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
- `reStructuredText/basic-syntax/2 <https://docutils.sourceforge.io/docs/user/rst/quickref.html>`_

Then you can save it commiting the changes using *Gitlab portal* or *git* commands.

Integrate diagrams
++++++++++++++++++

To integrate diagrams, you can encode your diagram using textual description in Kroki (See `Kroki <https://kroki.io/>`_)

To enter the description of your diagram in your documentation you have to use a kroki section done this way :

**For reStructuredText file :**

.. code-block:: reStructuredText

  .. kroki::               
    :type: plantuml        
    The textual description of your diagram

Integrate images
++++++++++++++++

To integrate images in your documentation, you have to put the image file under the directory "doc/images".
Then you can use an **"image"** section this way :

.. code-block:: reStructuredText

  .. image:: images/<name_of_the_file.jpg>

Integrate draw.io graphics
++++++++++++++++++++++++++

To integrate draw.io graphics in your documentation, you have to put the graphic file under the directory "doc/graphics".
Then you can use an **"drawio-image"** section this way :

.. code-block:: reStructuredText

  .. drawio-image:: graphics/<name_of_the_file.drawio>
    
Review of documentation
***********************

Each time you commit your files in the branch, the check pipeline is launched on your branch.
The merge request you have created is updated.
In this review request, you can view the documentation assotiated to the last commit.

To do it, you have to: 

- Select the merge request assotiated to your branch : Select **"Merge requests"** in left menu. Then click on the merge request you created for this branch button.

.. image:: images/Gitlab_Review.jpg

- In your merge request you can view your documentation clicking in **ViewApp** button.

.. image:: images/Gitlab_Review_2.jpg

- You can see your documentation.

.. image:: images/Gitlab_Review_3.jpg

FYI : If if you push new changes in the branch, the HTML pages will be updated on server. Hence you will have to refresh the current page in your browser by clicking CTRL + F5.

- If the documentation is OK you can approve the merge request (click on **Approve** button).

.. image:: images/Gitlab_Review_2.jpg

- Then you have to launch the **GATE** pipeline. Select the **"Pipelines"** tab in your merge request and click on **"Run pipeline"** button.

.. image:: images/Gitlab_Review_5.jpg

- Then select the new created pipeline and click on **"Stop"** button.

.. image:: images/Gitlab_Review_6.jpg

- And now you can merge by click on **Merge** button in your merge request overview

.. image:: images/Gitlab_Review_4.jpg

Publication of documentation
****************************

The **BUILD** of the documentation in **html pages** is done using Sphinx during pipeline execution.
It is built in following pipeline :

- CHECK pipeline during a merge request
- GATE pipeline at merge request approval
- LATEST pipeline when the merge is done into main branch
- RELEASE pipeline on tag

The **PUBLISH** of the page to the documentation server is done during the deploy job of each pipeline stages.

.. kroki::
  :type: plantuml

  "Module\nto document" -> "Markdown\nSource" : Create\nand\nEdit
  "Markdown\nSource" -> "Html\npage" : Build
  "Html\npage" -> "Documentation\nserver" : publish

Official documentation is published when a tag is put on main branch.

**Documentation is published under :**

For latest tag of the branch
http://sr-f1-gitpages.eki.ekinops.com/<project_id>/<branch>/stable

For latest commit in the branch
http://sr-f1-gitpages.eki.ekinops.com/<project_id>/<branch>/latest

For a given tag
http://sr-f1-gitpages.eki.ekinops.com/<project_id>/<branch>/<tag>

Publication on Ekipedia
***********************

In parallel with the **html** one, it is possible to setup a publication on **Ekipedia**.

The publication is made at the following steps:

- LATEST pipeline when the merge is done into main branch
- RELEASE pipeline on tag

It is available into **5 - R&D: Development** space (https://ekipedia.ekinops.com/display/RDDev).

To enable it, the **variables** below need to be defined in your Gitlab project (https://gitlab.ekinops.com/< project path >/-/settings/ci_cd , "Variables" item) :

* **EKIPEDIA_TOPIC_PAGE** - Required : fill with one of the following topic pages defined just under RDDev space

  * 00-Guidelines & Rules (Brice, Priscila, Didier, Milas)

  * 01-Access Solutions & Products (Christian P)

  * 02-SDx & Cloud Solutions & Products (Vincent J.)
  
  * 03-Transport Solutions and Products (Claude and Priscila)

  * 04-Platforms and Infra (Didier and Milas)

  * 06-PMO & CPM (guidelines) (Kris)

* **EKIPEDIA_PARENT_PAGE** - Optional : the parent page name of your documentation, defined just under EKIPEDIA_TOPIC_PAGE. You can define this field as you want; if it is left empty, the parent page name will be the Gitlab project name. It is however recommended to set a parent page name of your own.

Then, the documentation is published under **RDDev space, $EKIPEDIA_TOPIC_PAGE topic and $EKIPEDIA_PARENT_PAGE parent page**.

There is currently **no support of different versions** of the same documentation : each time the documentation is pushed, the pages with the same name will be overriden, but the existing pages not included into the latest publication will not be removed.

If you want to temporarily **disable the Ekipedia publication** without removing these variables, you can set the following variable in your Gitlab project :

**DOCASCODE_EKIPEDIA_ENABLED** : set to "N" value in order to disable the Ekipedia publication; set to "Y", leave empty or remove the variable to re-enable it.

*Example :*

The current documentation page https://ekipedia.ekinops.com/display/RDDev/How+to+create+new+documentation+project is published under RDDev space key, and the following page hierarchy

::

    5 - R&D: Development                                   # space name
    00-Guidelines & Rules (Brice, Priscila, Didier, Milas) # topic page name
    DevSecOps documentation                                # parent page name
    Welcome to DevSecOps documentation                     # project index.rst page name
    DevSecOps HowTo                                        # current .rst page
